#ifndef _H_PROTOCOL_PROCESS_
#define _H_PROTOCOL_PROCESS_

#include "tim/Timer.hpp"
#include "rf/proto/Protocol.hpp"

class ProtocolProcess : public TimerCallback
{
private:
	Protocol &protocol;

public:
	ProtocolProcess();
	~ProtocolProcess();

	void execute();

};

#endif
