#ifndef _H_RADIO_STATUS_
#define _H_RADIO_STATUS_

#include "rf/CC1101.hpp"
#include "rf/CC1101Conf.hpp"
#include "tim/Timer.hpp"
#include "gpio/GPIO.hpp"

class RadioStatus: public TimerCallback
{
private:
	CC1101 &cc;
	MARCSTATE state;
	RSSI rssi;
	PKTSTATUS pktstat;
	RXBYTES rxbytes;
	TXBYTES txbytes;
	LQI quality;

	FIFO fifo;

	SIDLE sidle;
	SFRX sfrx;
	SFTX sftx;
	SRX srx;

	RadioStatus() = delete;

public:
	RadioStatus(CC1101 &cc);
	~RadioStatus();

	void execute();
};

#endif
