#ifndef _H_SYS_LED_
#define _H_SYS_LED_

#include "gpio/GPIO.hpp"
#include "tim/Timer.hpp"

/**
 * Class which blinks with SysLed, to show that something is
 * happening - system ready
 */
class SysLed : public TimerCallback
{
private:
	bool status = false;
	uint16_t pin = GPIO_Pin_13;
	GPIO_TypeDef *port = GPIOC;
	GPIO sysGPIO;

public:
	SysLed();
	~SysLed();

	void execute();
};

#endif
