#ifndef _H_TEMP_
#define _H_TEMP_

#include "sensors/temp/NTCTemperature.hpp"
#include "tim/Timer.hpp"

class Temp : public TimerCallback
{
private:
	enum STATE {ENABLE, READ_TEMP};
	// once per minute
	const uint16_t timeout = 6000;
	const uint16_t waitToMeasure = 10;
	NTCTemperature ntc;
	Timer &tim;
	STATE state;

public:
	Temp();
	~Temp();

	void execute();

};

#endif

