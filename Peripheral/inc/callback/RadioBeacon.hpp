#ifndef _H_RADIO_BEACON_
#define _H_RADIO_BEACON_

#include "rf/CC1101.hpp"
#include "rf/CC1101Conf.hpp"
#include "rf/proto/Protocol.hpp"
#include "tim/Timer.hpp"


class RadioBeacon : public TimerCallback
{
private:
	CC1101 &cc;
	Protocol &protocol;
	FIFO fifo;
	STX stx;
	SIDLE sidle;
	Message message;
	RadioBeacon() = delete;

public:
	RadioBeacon(CC1101 &cc);
	~RadioBeacon();

	void execute();
};

#endif
