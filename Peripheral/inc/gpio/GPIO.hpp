#ifndef _H_GPIO_
#define _H_GPIO_

#include "stm/stm32f10x_gpio.h"
#include "tim/Timer.hpp"

#define LEVEL_LOW	0
#define LEVEL_HIGH	1

class GPIO
{
private:
	GPIO_InitTypeDef pinStruct;
	GPIO_TypeDef *port;

	GPIO();

public:
	GPIO(GPIO_TypeDef *port, uint16_t pin);
	~GPIO();

	void setADCInput();
	void setOutputPP();
	void setInput(uint8_t pullUp);
	void setLevel(uint8_t level);
	void setInterrupt();
	uint8_t getLevel();
};

#endif
