#ifndef _H_GPIO_INTERRUPT_
#define _H_GPIO_INTERRUPT_

#include <stdint.h>
#include "stm/stm32f10x_exti.h"

/**
 * Register GPIO interrupt handler, type of interrupt etc
 */
class GPIOInterrupt
{
private:
	GPIOInterrupt() = delete;

public:
	GPIOInterrupt(uint32_t extiLine, uint16_t nvicChannel);
	~GPIOInterrupt();
};

#endif
