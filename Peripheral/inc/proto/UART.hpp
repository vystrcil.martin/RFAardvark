#ifndef _H_UART_
#define _H_UART_

#include "stm/stm32f10x_conf.h"

class UART
{
private:
	UART();

public:
	~UART();

	static UART &getInstance();
	void sendMessage(const char *message, uint16_t length);
};

#endif
