#ifndef _H_DBG_LOG_
#define _H_DBG_LOG_

#include "proto/UART.hpp"

extern void debugStr(const char *message);
extern void printLine(const char *message);
extern void printNr(uint32_t num);
extern void printHex(uint8_t num);
extern void printHex(uint16_t num);

#endif
