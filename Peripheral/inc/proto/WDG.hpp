#ifndef _H_WDG_
#define _H_WDG_

#include "stm/stm32f10x_conf.h"

class WDG
{
private:
	WDG();

public:
	~WDG();

	static WDG &getInstance();
	void refresh();
};

#endif
