#ifndef _H_SPI_
#define _H_SPI_

#include "gpio/GPIO.hpp"

#include "stm/stm32f10x.h"

#define PIN_SS		GPIO_Pin_4
#define PIN_CLK		GPIO_Pin_5
#define PIN_MISO	GPIO_Pin_6
#define PIN_MOSI	GPIO_Pin_7

#define SPI_CLK		RCC_APB2Periph_SPI1
#define SPI_IFACE	SPI1

/**
 * Represents SPI connection towards radio
 */
class SPI
{
private:
	uint16_t rxBuffer[64];
	uint16_t txBuffer[64];
	GPIO chipSelect;

public:
	SPI();
	~SPI();

	uint8_t sendRead(uint8_t reg);
	void sendWrite(uint8_t reg, uint8_t value);

	void sendBurst(uint8_t reg, uint8_t *data, uint8_t len);
	void readBurst(uint8_t reg, uint8_t *data, uint8_t len);
};

#endif
