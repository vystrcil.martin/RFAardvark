#ifndef _H_CC11010_
#define _H_CC11010_

#include <rf/CC1101Regs.hpp>
#include "proto/SPI.hpp"

class CC1101
{
private:
	SPI spi;
	CC1101();

public:
	~CC1101();

	static CC1101 &getInstance();

	uint8_t sendCmd(CMD command);
	uint8_t readReg(REG reg);
	void writeReg(REG reg, uint8_t data);

	void sendData(uint8_t *data, uint8_t len);
	void readData(uint8_t *data, uint8_t len);

};

#endif
