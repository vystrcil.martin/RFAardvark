#ifndef _H_CC1101_CONF_
#define _H_CC1101_CONF_

#include <rf/CC1101Regs.hpp>
#include "rf/CC1101.hpp"
#include "stm/stm32f10x.h"
#include  "proto/Log.hpp"

class CC1101Conf
{
private:
	CC1101Conf() = delete;

protected:
	uint8_t reg;
	CC1101 &cc;
	REG addr;

public:
	CC1101Conf(CC1101 &cc, REG addr) :
			reg(0), cc(cc), addr(addr)
	{
		this->load();
	}

	~CC1101Conf()
	{
	}

	/**
	 * Load register value from hardware
	 */
	void load()
	{
		reg = cc.readReg(addr);
	}

	/**
	 * Set register value to hardware
	 */
	void set()
	{
		cc.writeReg(addr, reg);
		this->load();
	}

	uint8_t get()
	{
		return reg;
	}
};

class CC1101Cmd
{
private:
	CC1101Cmd() = delete;

protected:
	CC1101 &cc;
	CMD cmd;

public:
	CC1101Cmd(CC1101 &cc, CMD cmd) :
			cc(cc), cmd(cmd)
	{

	}

	~CC1101Cmd()
	{
	}

	/**
	 * Send command to hardware, read response back
	 */
	uint8_t send()
	{
		return cc.sendCmd(cmd);
	}

	void sendBcast(uint8_t *byte, uint8_t len)
	{
		return cc.sendData(byte, len);;
	}
};

class IOCFG2: public CC1101Conf
{
private:
	IOCFG2() = delete;

public:
	bool GDO2_INV = false;		// Invert output
	uint8_t GDO2_CFG = 0x29;	// Output pin configuration

	IOCFG2(CC1101 &cc) :
			CC1101Conf(cc, REG__IOCFG2)
	{
	}

	~IOCFG2()
	{
	}

	void load()
	{
		CC1101Conf::load();
		GDO2_INV = (reg & (1 << 6)) ? true : false;
		GDO2_CFG = (reg & 0x2F);
	}

	void set()
	{
		reg = (GDO2_INV ? 0x1 : 0x0) << 6;
		reg |= (GDO2_CFG & 0x2F);
		CC1101Conf::set();
	}
};

class IOCFG1: public CC1101Conf
{
private:
	IOCFG1() = delete;

public:
	bool GDO_DS = false;			// Output drive strength
	bool GDO1_INV = false;		// Invert output
	uint8_t GDO1_CFG = 0x2E;	// Output pin configuration

	IOCFG1(CC1101 &cc) :
			CC1101Conf(cc, REG__IOCFG1)
	{
	}

	~IOCFG1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		GDO_DS = (reg & (1 << 7)) ? true : false;
		GDO1_INV = (reg & (1 << 6)) ? true : false;
		GDO1_CFG = (reg & 0x2F);
	}

	void set()
	{
		reg = (GDO_DS ? 0x1 : 0x0) << 7;
		reg |= (GDO1_INV ? 0x1 : 0x0) << 6;
		reg |= (GDO1_CFG & 0x2F);
		CC1101Conf::set();
	}
};

class IOCFG0: public CC1101Conf
{
private:
	IOCFG0() = delete;

public:
	bool TEMP_SENSOR_ENABLE = false;	// Temperature sensor enable
	bool GDO0_INV = false;				// Invert output
	uint8_t GDO0_CFG = 0x3F;			// Output pin configuration

	IOCFG0(CC1101 &cc) :
			CC1101Conf(cc, REG__IOCFG0)
	{
	}

	~IOCFG0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		TEMP_SENSOR_ENABLE = (reg & (1 << 7)) ? true : false;
		GDO0_INV = (reg & (1 << 6)) ? true : false;
		GDO0_CFG = (reg & 0x2F);
	}

	void set()
	{
		reg = (TEMP_SENSOR_ENABLE ? 0x1 : 0x0) << 7;
		reg |= (GDO0_INV ? 0x1 : 0x0) << 6;
		reg |= (GDO0_CFG & 0x2F);
		CC1101Conf::set();
	}
};

class FIFOTHR: public CC1101Conf
{
private:
	FIFOTHR() = delete;

public:
	bool ADC_RETENTION = false;	// If RX filter below 325kHz, set to 1 before SLEEP
	uint8_t CLOSE_IN_RX = 0x0;		// Rx Attenuation
									// 0x0 - 0dB
									// 0x1 - 6dB
									// 0x2 - 12dB
									// 0x3 - 18dB
	uint8_t FIFO_THR = 0x7;		// Set threshold for TX / RX FIFO
									// 0x0 - 61 / 4
									// 0x5 - 41 / 24
									// 0x9 - 25 / 40
									// 0xF - 1 / 64

	FIFOTHR(CC1101 &cc) :
			CC1101Conf(cc, REG__FIFOTHR)
	{
	}

	~FIFOTHR()
	{
	}

	void load()
	{
		CC1101Conf::load();
		ADC_RETENTION = (reg & (1 << 7)) ? true : false;
		CLOSE_IN_RX = ((reg >> 4) & 0x3);
		FIFO_THR = reg & 0xF;
	}

	void set()
	{
		reg = (ADC_RETENTION ? 0x1 : 0x0) << 7;
		reg |= (CLOSE_IN_RX & 0x3) << 4;
		reg |= (FIFO_THR & 0xF);
		CC1101Conf::set();
	}
};

class SYNC1: public CC1101Conf
{
private:
	SYNC1() = delete;

public:
	uint8_t DATA = 0xD3;	// SYNC word high byte

	SYNC1(CC1101 &cc) :
			CC1101Conf(cc, REG__SYNC1)
	{
	}

	~SYNC1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		DATA = reg;
	}

	void set()
	{
		reg = DATA;
		CC1101Conf::set();
	}
};

class SYNC0: public CC1101Conf
{
private:
	SYNC0() = delete;

public:
	uint8_t DATA = 0x91;	// SYNC word high byte

	SYNC0(CC1101 &cc) :
			CC1101Conf(cc, REG__SYNC0)
	{
	}

	~SYNC0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		DATA = reg;
	}

	void set()
	{
		reg = DATA;
		CC1101Conf::set();
	}
};

class PKTLEN: public CC1101Conf
{
private:
	PKTLEN() = delete;

public:
	uint8_t LEN = 0xFF;		// Length of packet

	PKTLEN(CC1101 &cc) :
			CC1101Conf(cc, REG__PKTLEN)
	{
	}

	~PKTLEN()
	{
	}

	void load()
	{
		CC1101Conf::load();
		LEN = reg;
	}

	void set()
	{
		reg = LEN;
		CC1101Conf::set();
	}
};

class PKTCTRL1: public CC1101Conf
{
private:
	PKTCTRL1() = delete;

public:
	uint8_t PQT = 0x0;
	bool CRC_AUTOFLUSH = false;
	bool APPEND_STATUS = true;
	uint8_t ADR_CHK = 0x0;	// Control address check config
							// 0x0 - No address check
							// 0x1 - Address check, no bcast
							// 0x2 - Address check, 0x0 bcast
							// 0x3 - Address check, 0x0, 0xFF bcast

	PKTCTRL1(CC1101 &cc) :
			CC1101Conf(cc, REG__PKTCTRL1)
	{
	}

	~PKTCTRL1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		PQT = ((reg >> 5) & 0x3);
		CRC_AUTOFLUSH = ((reg >> 3) & 0x1) ? true : false;
		APPEND_STATUS = ((reg >> 2) & 0x1) ? true : false;
		ADR_CHK = (reg & 0x3);
	}

	void set()
	{
		reg = (PQT & 0x3) << 5;
		reg |= (CRC_AUTOFLUSH ? 0x1 : 0x0) << 3;
		reg |= (APPEND_STATUS ? 0x1 : 0x0) << 2;
		reg |= (ADR_CHK & 0x3);
		CC1101Conf::set();
	}

};

class PKTCTRL0: public CC1101Conf
{
private:
	PKTCTRL0() = delete;

public:
	bool WHITE_DATA = true;	// Data whitening
	uint8_t PKT_FMT = 0x0;	// Packet format
							// 0x0 - Normal mode, FIFO for RX & TX
							// 0x1 - Synchronous serial - Data in on GDO0, Data out on GDOx
							// 0x2 - Random TX, PN9 generator
							// 0x3 - Async serial - Data in on GDO0, Data out on GDOx
	bool CRC_EN = true;		// CRC check on RX, add on TX
	uint8_t LEN_CFG = 0x1;	// Configure packet length
							// 0x0 - fixed pkt len, check PKTLEN register
							// 0x1 - variable packet length - first byte after SYNC
							// 0x2 - infinite packet length

	PKTCTRL0(CC1101 &cc) :
			CC1101Conf(cc, REG__PKTCTRL0)
	{
	}

	~PKTCTRL0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		WHITE_DATA = ((reg >> 6) & 0x1) ? true : false;
		PKT_FMT = ((reg >> 4) & 0x3);
		CRC_EN = ((reg >> 2) & 0x1) ? true : false;
		LEN_CFG = (reg & 0x3);
	}

	void set()
	{
		reg = (WHITE_DATA ? 0x1 : 0x0) << 6;
		reg |= (PKT_FMT & 0x3) << 4;
		reg |= (CRC_EN ? 0x1 : 0x0) << 2;
		reg |= (LEN_CFG & 0x3);
		CC1101Conf::set();
	}
};

class ADDR: public CC1101Conf
{
private:
	ADDR() = delete;

public:
	uint8_t DEVICE_ADDR = 0x0;	// Address used for packet filtration

	ADDR(CC1101 &cc) :
			CC1101Conf(cc, REG__ADDR)
	{
	}

	~ADDR()
	{
	}

	void load()
	{
		CC1101Conf::load();
		DEVICE_ADDR = reg;
	}

	void set()
	{
		reg = DEVICE_ADDR;
		CC1101Conf::set();
	}
};

class CHANNR: public CC1101Conf
{
private:
	CHANNR() = delete;

public:
	uint8_t CHAN = 0x0;		// Channel to be used

	CHANNR(CC1101 &cc) :
			CC1101Conf(cc, REG__CHANNR)
	{
	}

	~CHANNR()
	{
	}

	void load()
	{
		CC1101Conf::load();
		CHAN = reg;
	}

	void set()
	{
		reg = CHAN;
		CC1101Conf::set();
	}
};

class FSCTRL1: public CC1101Conf
{
private:
	FSCTRL1() = delete;

public:
	uint8_t FREQ_IF = 0xF;	// IF Frequency for RX

	FSCTRL1(CC1101 &cc) :
			CC1101Conf(cc, REG__FSCTRL1)
	{
	}

	~FSCTRL1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FREQ_IF = reg;
	}

	void set()
	{
		reg = FREQ_IF;
		CC1101Conf::set();
	}
};

class FSCTRL0: public CC1101Conf
{
private:
	FSCTRL0() = delete;

public:
	uint8_t FREQOFF = 0x0;	// Frequency offset added to base +-210kHz

	FSCTRL0(CC1101 &cc) :
			CC1101Conf(cc, REG__FSCTRL0)
	{
	}

	~FSCTRL0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FREQOFF = reg;
	}

	void set()
	{
		reg = FREQOFF;
		CC1101Conf::set();
	}
};

class FREQ2: public CC1101Conf
{
private:
	FREQ2() = delete;

public:
	uint8_t FREQ = 0x1E;	// Base frequency

	FREQ2(CC1101 &cc) :
			CC1101Conf(cc, REG__FREQ2)
	{
	}

	~FREQ2()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FREQ = (reg & 0x1F);
	}

	void set()
	{
		reg = (FREQ & 0x1F);
		CC1101Conf::set();
	}
};

class FREQ1: public CC1101Conf
{
private:
	FREQ1() = delete;

public:
	uint8_t FREQ = 0xC4;	// Base frequency

	FREQ1(CC1101 &cc) :
			CC1101Conf(cc, REG__FREQ1)
	{
	}

	~FREQ1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FREQ = reg;
	}

	void set()
	{
		reg = FREQ;
		CC1101Conf::set();
	}
};

class FREQ0: public CC1101Conf
{
private:
	FREQ0() = delete;

public:
	uint8_t FREQ = 0xEC;	// Base frequency

	FREQ0(CC1101 &cc) :
			CC1101Conf(cc, REG__FREQ0)
	{
	}

	~FREQ0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FREQ = reg;
	}

	void set()
	{
		reg = FREQ;
		CC1101Conf::set();
	}
};

class MDMCFG4: public CC1101Conf
{
private:
	MDMCFG4() = delete;

public:
	uint8_t CHANBW_E = 0x02;	//
	uint8_t CHANBW_M = 0x00;	// Set channel bandwidth - equation in datasheet
	uint8_t DRATE_E = 0x0C;		// Exponent of user specified symbol rate

	MDMCFG4(CC1101 &cc) :
			CC1101Conf(cc, REG__MDMCFG4)
	{
	}

	~MDMCFG4()
	{
	}

	void load()
	{
		CC1101Conf::load();
		CHANBW_E = ((reg >> 6) & 0x3);
		CHANBW_M = ((reg >> 4) & 0x3);
		DRATE_E = (reg & 0xF);
	}

	void set()
	{
		reg = (CHANBW_E & 0x3) << 6;
		reg |= (CHANBW_M & 0x3) << 4;
		reg |= (DRATE_E & 0xF);
		CC1101Conf::set();
	}
};

class MDMCFG3: public CC1101Conf
{
private:
	MDMCFG3() = delete;

public:
	uint8_t DRATE_M = 0x22;		// Mantisa of user specified symbol rate

	MDMCFG3(CC1101 &cc) :
			CC1101Conf(cc, REG__MDMCFG3)
	{
	}

	~MDMCFG3()
	{
	}

	void load()
	{
		CC1101Conf::load();
		DRATE_M = reg;
	}

	void set()
	{
		reg = DRATE_M;
		CC1101Conf::set();
	}
};

class MDMCFG2: public CC1101Conf
{
private:
	MDMCFG2() = delete;

public:
	bool DEM_DCFILT_OFF = false;	// Disable DC blocking filter
	uint8_t MOD_FMT = 0x0;			// Modulation format
	bool MANCHESTER_EN = false;		// Enable Manchaster coding
	uint8_t SYNC_MODE = 0x2;		// SYNC word qualifier mode

	MDMCFG2(CC1101 &cc) :
			CC1101Conf(cc, REG__MDMCFG2)
	{
	}

	~MDMCFG2()
	{
	}

	void load()
	{
		CC1101Conf::load();
		DEM_DCFILT_OFF = ((reg >> 7) & 0x1) ? true : false;
		MOD_FMT = ((reg >> 4) & 0x7);
		MANCHESTER_EN = ((reg >> 3) & 0x1) ? true : false;
		SYNC_MODE = (reg & 0x7);
	}

	void set()
	{
		reg = (DEM_DCFILT_OFF ? 0x1 : 0x0) << 7;
		reg |= (MOD_FMT & 0x7) << 4;
		reg |= (MANCHESTER_EN ? 0x1 : 0x0) << 3;
		reg |= (SYNC_MODE & 0x7);
		CC1101Conf::set();
	}
};

class MDMCFG1: public CC1101Conf
{
private:
	MDMCFG1() = delete;

public:
	bool FEC_ENABLE = false;		// Enable Forward Error Connection
	uint8_t NUM_PREAMBLE = 0x2;		// Minimum number of preamble bytes
	uint8_t CHANSPC_E = 0x2;		// 2-bit exponent of channel spacing

	MDMCFG1(CC1101 &cc) :
			CC1101Conf(cc, REG__MDMCFG1)
	{
	}

	~MDMCFG1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FEC_ENABLE = ((reg >> 7) & 0x1) ? true : false;
		NUM_PREAMBLE = ((reg >> 4) & 0x7);
		CHANSPC_E = (reg & 0x3);
	}

	void set()
	{
		reg = (FEC_ENABLE ? 0x1 : 0x0) << 7;
		reg |= (NUM_PREAMBLE & 0x7) << 4;
		reg |= (CHANSPC_E & 0x3);
		CC1101Conf::set();
	}
};

class MDMCFG0: public CC1101Conf
{
private:
	MDMCFG0() = delete;

public:
	uint8_t CHANSPC_M = 0xF8;	// 8-bit mantissa of channel spacing

	MDMCFG0(CC1101 &cc) :
			CC1101Conf(cc, REG__MDMCFG0)
	{
	}

	~MDMCFG0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		reg = CHANSPC_M;
	}

	void set()
	{
		reg = CHANSPC_M;
		CC1101Conf::set();
	}
};

class DEVIATN: public CC1101Conf
{
private:
	DEVIATN() = delete;

public:
	uint8_t DEVIATION_E = 0x4;	// Deviation exponent
	uint8_t DEVIATION_M = 0x7;	// Deviation mantissa

	DEVIATN(CC1101 &cc) :
			CC1101Conf(cc, REG__DEVIATN)
	{
	}

	~DEVIATN()
	{
	}

	void load()
	{
		CC1101Conf::load();
		DEVIATION_E = ((reg >> 4) & 0x7);
		DEVIATION_M = (reg & 0x7);
	}

	void set()
	{
		reg = (DEVIATION_E & 0x7) << 4;
		reg |= (DEVIATION_M & 0x7);
		CC1101Conf::set();
	}
};

class MCSM2: public CC1101Conf
{
private:
	MCSM2() = delete;

public:
	bool RX_TIME_RSSI = false;	// RX termination based on RSSI
	bool RX_TIME_QUAL = false;	// Check something after RX_TIME expires
	uint8_t RX_TIME = 0x7;// Timeout for sync word search in RX - relative to EVENT0

	MCSM2(CC1101 &cc) :
			CC1101Conf(cc, REG__MCSM2)
	{
	}

	~MCSM2()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RX_TIME_RSSI = ((reg >> 4) & 0x1) ? true : false;
		RX_TIME_QUAL = ((reg >> 3) & 0x1) ? true : false;
		RX_TIME = (reg & 0x7);
	}

	void set()
	{
		reg = (RX_TIME_RSSI ? 0x1 : 0x0) << 4;
		reg |= (RX_TIME_QUAL ? 0x1 : 0x0) << 3;
		reg |= (RX_TIME & 0x7);
		CC1101Conf::set();
	}
};

class MCSM1: public CC1101Conf
{
private:
	MCSM1() = delete;

public:
	uint8_t CCA_MODE = 0x3;		// Clear Channel Assignment
	uint8_t RXOFF_MODE = 0x0;	// What should happen when packet received
								// 0x0 - IDLE
								// 0x1 - FSTXON
								// 0x2 - TX
								// 0x3 - Stay in RX
	uint8_t TXOFF_MODE = 0x0;	// What should happen when packet sent
								// 0x0 - IDLE
								// 0x1 - FSTXON
								// 0x2 - Stay in TX
								// 0x3 - RX

	MCSM1(CC1101 &cc) :
			CC1101Conf(cc, REG__MCSM1)
	{
	}

	~MCSM1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		CCA_MODE = ((reg >> 4) & 0x3);
		RXOFF_MODE = ((reg >> 2) & 0x3);
		TXOFF_MODE = (reg & 0x3);
	}

	void set()
	{
		reg = (CCA_MODE & 0x3) << 4;
		reg |= (RXOFF_MODE & 0x3) << 2;
		reg |= (TXOFF_MODE & 0x3);
		CC1101Conf::set();
	}
};

class MCSM0: public CC1101Conf
{
private:
	MCSM0() = delete;

public:
	uint8_t FS_AUTOCAL = 0x0;		// Automatically calibrate
	uint8_t PO_TIMEOUT = 0x1;		// Startup timeout (XOSF cycles)
	bool PIN_CTRL_EN = false;		// Enable pin radio control
	bool XOSC_FORCE_ON = false;		// Force XOSC to stay on in SLEEP mode

	MCSM0(CC1101 &cc) :
			CC1101Conf(cc, REG__MCSM0)
	{
	}

	~MCSM0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FS_AUTOCAL = ((reg >> 4) & 0x3);
		PO_TIMEOUT = ((reg >> 2) & 0x3);
		PIN_CTRL_EN = ((reg >> 1) & 0x1) ? true : false;
		XOSC_FORCE_ON = (reg & 0x1) ? true : false;
	}

	void set()
	{
		reg = (FS_AUTOCAL & 0x3) << 4;
		reg |= (PO_TIMEOUT & 0x3) << 2;
		reg |= (PIN_CTRL_EN ? 0x1 : 0x0) << 1;
		reg |= (XOSC_FORCE_ON ? 0x1 : 0x0);
		CC1101Conf::set();
	}
};

class FOCCFG: public CC1101Conf
{
private:
	FOCCFG() = delete;

public:
	bool FOC_BS_CS_GATE = true;	// Freeze demodulator frequency offset until CS goes high
	uint8_t FOC_PRE_K = 0x2;// Frequency compensation loop gain before sync word
							// 0x0 - K
							// 0x1 - 2K
							// 0x2 - 3K
							// 0x3 - 4K
	uint8_t FOC_POST_K = 0x1;// Frequency compensation loop gain after sync word
							 // 0x0 - Same as FOC_PRE_K
							 // 0x1 - K/2
	uint8_t FOC_LIMIT = 0x2;// The saturation point for the frequency offset compensation algorithm
							// NOTE: ASK / OOK FOC_LIMIT = 0
							// 0x0 - no frequency offset
							// 0x1 - BW/8
							// 0x2 - BW/4
							// 0x3 - BW/2

	FOCCFG(CC1101 &cc) :
			CC1101Conf(cc, REG__FOCCFG)
	{
	}

	~FOCCFG()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FOC_BS_CS_GATE = ((reg & (1 << 5)) ? true : false);
		FOC_PRE_K = ((reg >> 3) & 0x3);
		FOC_POST_K = ((reg >> 2) & 0x1);
		FOC_LIMIT = (reg & 0x3);
	}

	void set()
	{
		reg = ((FOC_BS_CS_GATE & (1 << 5)) ? 0x1 : 0x0);
		reg |= (FOC_PRE_K & 0x3) << 3;
		reg |= (FOC_POST_K & 0x1) << 2;
		reg |= (FOC_LIMIT & 0x3);
		CC1101Conf::set();
	}
};

class BSCFG: public CC1101Conf
{
private:
	BSCFG() = delete;

public:
	uint8_t BS_PRE_KI = 0x1;// Clock recovery feedback loop integral gain before sync
							// 0x0 - K
							// 0x1 - 2K
							// 0x2 - 3K
							// 0x3 - 4K
	uint8_t BS_PRE_KP = 0x2;// Clock recovery feedback loop proportional gain before sync
							// 0x0 - K
							// 0x1 - 2K
							// 0x2 - 3K
							// 0x3 - 4K
	bool BS_POST_KI = true;	// Clock recovery feedback loop integral after sync
							// 0x0 - Same
							// 0x1 - K/2
	bool BS_POST_KP = true;	// Clock recovery feedback loop proportional after sync
							// 0x0 - Same
							// 0x1 - K/2
	uint8_t BS_LIMIT = 0x0;	// Saturation point for data rate offset compensation
							// 0x0 - No data rate compensation
							// 0x1 - 3.125% offset
							// 0x2 - 6.25% offset
							// 0x3 - 12.5% offset

	BSCFG(CC1101 &cc) :
			CC1101Conf(cc, REG__BSCFG)
	{
	}

	~BSCFG()
	{
	}

	void load()
	{
		CC1101Conf::load();
		BS_PRE_KI = ((reg >> 6) & 0x3);
		BS_PRE_KP = ((reg >> 4) & 0x3);
		BS_POST_KI = ((reg >> 3) & 0x1) ? true : false;
		BS_POST_KP = ((reg >> 2) & 0x1) ? true : false;
		BS_LIMIT = (reg & 0x3);
	}

	void set()
	{
		reg = (BS_PRE_KI & 0x3) << 6;
		reg |= (BS_PRE_KP & 0x3) << 4;
		reg |= (BS_POST_KI ? 0x1 : 0x0) << 3;
		reg |= (BS_POST_KP ? 0x1 : 0x0) << 2;
		reg |= (BS_LIMIT & 0x3);
		CC1101Conf::set();
	}
};

class AGCCTRL2: public CC1101Conf
{
private:
	AGCCTRL2() = delete;

public:
	uint8_t MAX_DVGA_GAIN = 0x0;	// Reduce max allowable DVGA gain
									// 0x0 - Allow all gain settings
									// 0x1 - The highest cannot be used
									// 0x2 - The 2nd highest cannot be used
									// 0x3 - The 3rd highest cannot be used
	uint8_t MAX_LNA_GAIN = 0x0;	// Set max LNA gain relatively to max possible
								// 0x0 - Maximum possible
								// ...
								// 0x7 - 17.1dB below maximum
	uint8_t MAGN_TARGET = 0x3;// Target value for averaged amplitude from digital channel filter
							  // 0x0 - 24dB
							  // 0x3 - 33dB
							  // 0x7 - 42dB

	AGCCTRL2(CC1101 &cc) :
			CC1101Conf(cc, REG__AGCTRL2)
	{
	}

	~AGCCTRL2()
	{
	}

	void load()
	{
		CC1101Conf::load();
		reg = (MAX_DVGA_GAIN & 0x3) << 6;
		reg |= (MAX_LNA_GAIN & 0x7) << 3;
		reg |= (MAGN_TARGET & 0x7);
	}

	void set()
	{
		reg = (MAX_DVGA_GAIN & 0x3) << 6;
		reg |= (MAX_LNA_GAIN & 0x7) << 3;
		reg |= (MAGN_TARGET & 0x7);
		CC1101Conf::set();
	}
};

class AGCCTRL1: public CC1101Conf
{
private:
	AGCCTRL1() = delete;

public:
	bool AGC_LNA_PRIORITY = true;
	uint8_t CARRIER_SENSE_REL_THR = 0x0;// Set relative change threshold for asserting carrier sense
										// 0x0 - relative CS disabled
										// 0x1 - 6dB increase in RSSI
										// 0x2 - 10dB increase in RSSI
										// 0x3 - 14dB increase in RSSI
	uint8_t CARRIER_SENSE_ABS_THR = 0x0;// Sets the absolute RSSI threshold for asserting carrier sense.
										// 0x0 - Use MAGN_TARGET settings
										// ....
										// 0x7 - 7dB above MAGN_TARGET
										// 0x8 - Carrier sense threshold disabled
										// 0x9 - 7dB below MAGN_TARGET
										// 0xF - 1dB below MAGN_TARGET

	AGCCTRL1(CC1101 &cc) :
			CC1101Conf(cc, REG__AGCTRL1)
	{
	}

	~AGCCTRL1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		AGC_LNA_PRIORITY = ((reg >> 6) & 0x1) ? true : false;
		CARRIER_SENSE_REL_THR = ((reg >> 4) & 0x3);
		CARRIER_SENSE_ABS_THR = (reg & 0xF);
	}

	void set()
	{
		reg = (AGC_LNA_PRIORITY ? 0x1 : 0x0) << 6;
		reg |= (CARRIER_SENSE_REL_THR & 0x3) << 4;
		reg |= (CARRIER_SENSE_ABS_THR & 0xF);
		CC1101Conf::set();
	}
};

class AGCCTRL0: public CC1101Conf
{
private:
	AGCCTRL0() = delete;

public:
	uint8_t HYST_LEVEL = 0x2;	// Set the level of hysteresis on the magnitude
								// 0x0 - No hysteresis
								// 0x1 - Low hysteresis
								// 0x2 - Medium hysteresis
								// 0x3 - Large hysteresis
	uint8_t WAIT_TIME = 0x1;// Sets the number of channel filter samples from a gain adj
							// 0x0 - 8
							// 0x1 - 16
							// 0x2 - 24
							// 0x3 - 32
	uint8_t AGC_FREEZE = 0x0;	// Control when the AGC gain should be frozen
								// 0x0 - Adjust gain when required
								// 0x1 - Gain frozen when sync word found
								// 0x2 - Manually freeze the analogue gain, continue with digital
								// 0x3 - Manually freeze both gains
	uint8_t FILTER_LENGTH = 0x1;// Sets the averaging length for the amplitude from the channel
								// 0x0 - 8 samples / 4dB
								// 0x1 - 16 samples / 8dB
								// 0x2 - 32 samples / 12dB
								// 0x3 - 64 samples / 16dB

	AGCCTRL0(CC1101 &cc) :
			CC1101Conf(cc, REG__AGCTRL0)
	{
	}

	~AGCCTRL0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		HYST_LEVEL = ((reg >> 6) & 0x3);
		WAIT_TIME = ((reg >> 4) & 0x3);
		AGC_FREEZE = ((reg >> 2) & 0x3);
		FILTER_LENGTH = (reg & 0x3);
	}

	void set()
	{
		reg = (HYST_LEVEL & 0x3) << 6;
		reg |= (WAIT_TIME & 0x3) << 4;
		reg |= (AGC_FREEZE & 0x3) << 2;
		reg |= (FILTER_LENGTH & 0x3);
		CC1101Conf::set();
	}
};

class WOREVT1: public CC1101Conf
{
private:
	WOREVT1() = delete;

public:
	uint8_t EVENT0 = 0x87;	// High byte of EVENT0 timeout register

	WOREVT1(CC1101 &cc) :
			CC1101Conf(cc, REG__WOREVT1)
	{
	}

	~WOREVT1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		EVENT0 = reg;
	}

	void set()
	{
		reg = EVENT0;
		CC1101Conf::set();
	}
};

class WOREVT0: public CC1101Conf
{
private:
	WOREVT0() = delete;

public:
	uint8_t EVENT0 = 0x6B;	// Low byte of EVENT0 timeout register

	WOREVT0(CC1101 &cc) :
			CC1101Conf(cc, REG__WOREVT0)
	{
	}

	~WOREVT0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		EVENT0 = reg;
	}

	void set()
	{
		reg = EVENT0;
		CC1101Conf::set();
	}
};

class WORCTRL: public CC1101Conf
{
private:
	WORCTRL() = delete;

public:
	bool RC_PD = true;		// Power down signal to RC oscillator
	uint8_t EVENT1 = 0x7;	// Timeout setting from register block
							// 0x0 - 0.1 ms
							// 0x3 - 0.3 ms
							// 0x7 - 1.3 ms
	bool RC_CAL = true;		// Enable the RC oscillator calibration
	uint8_t WOR_RES = 0x0;	// Event0 resolution, maximal timeout
							// 0x0 - 2s
							// 0x1 - 60s
							// 0x2 - 31 min
							// 0x3 - 17 h

	WORCTRL(CC1101 &cc) :
			CC1101Conf(cc, REG__WORCTRL)
	{
	}

	~WORCTRL()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RC_PD = ((reg >> 7) & 0x1) ? true : false;
		EVENT1 = ((reg >> 4) & 0x3);
		RC_CAL = ((reg >> 3) & 0x1) ? true : false;
		WOR_RES = (reg & 0x3);
	}

	void set()
	{
		reg = (RC_PD ? 0x1 : 0x0) << 7;
		reg |= (EVENT1 & 0x3) << 4;
		reg |= (RC_CAL ? 0x1 : 0x0) << 3;
		reg |= (WOR_RES & 0x3);
		CC1101Conf::set();
	}
};

class FREND1: public CC1101Conf
{
private:
	FREND1() = delete;

public:
	uint8_t LNA_CURRENT = 0x1;		// Adjust front-end LNA PTAT current output
	uint8_t LNA2MIX_CURRENT = 0x1;		// Adjust front-end PTAT outputs
	uint8_t LODIV_BUF_CURRENT_RX = 0x1;	// Adjust current in RX LO buffer
	uint8_t MIX_CURRENT = 0x2;			// Adjust current in mixer

	FREND1(CC1101 &cc) :
			CC1101Conf(cc, REG__FREND1)
	{
	}

	~FREND1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		LNA_CURRENT = ((reg >> 6) & 0x3);
		LNA2MIX_CURRENT = ((reg >> 4) & 0x3);
		LODIV_BUF_CURRENT_RX = ((reg >> 2) & 0x3);
		MIX_CURRENT = (reg & 0x3);
	}

	void set()
	{
		reg = (LNA_CURRENT & 0x3) << 6;
		reg |= (LNA2MIX_CURRENT & 0x3) << 4;
		reg |= (LODIV_BUF_CURRENT_RX & 0x3) << 2;
		reg |= (MIX_CURRENT & 0x3);
		CC1101Conf::set();
	}
};

class FREND0: public CC1101Conf
{
private:
	FREND0() = delete;

public:
	uint8_t LODIV_BUF_CURRENT_TX = 0x1;	// Adjust current TX LO buffer
	uint8_t PA_POWER = 0x0;		// Selects PA power settings - index to PATABLE

	FREND0(CC1101 &cc) :
			CC1101Conf(cc, REG__FREND0)
	{
	}

	~FREND0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		LODIV_BUF_CURRENT_TX = ((reg >> 4) & 0x3);
		PA_POWER = (reg & 0x3);
	}

	void set()
	{
		reg = (LODIV_BUF_CURRENT_TX & 0x3) << 4;
		reg |= (PA_POWER & 0x3);
		CC1101Conf::set();
	}
};

class FSCAL3: public CC1101Conf
{
private:
	FSCAL3() = delete;

public:
	uint8_t FSCAL3_VAL = 0x2;		// Frequency synthesizer calibration conf
	uint8_t CHP_CURR_CAL_EN = 0x2;	// Disable charge pump calibration when 0
	uint8_t FSCAL3_RES = 0x9;		// Frequency synthesizer calibration result

	FSCAL3(CC1101 &cc) :
			CC1101Conf(cc, REG__FSCAL3)
	{
	}

	~FSCAL3()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FSCAL3_VAL = ((reg >> 6) & 0x3);
		CHP_CURR_CAL_EN = ((reg >> 4) & 0x3);
		FSCAL3_RES = (reg & 0x3);
	}

	void set()
	{
		reg = (FSCAL3_VAL & 0x3) << 6;
		reg |= (CHP_CURR_CAL_EN & 0x3) << 4;
		reg |= (FSCAL3_RES & 0x3);
		CC1101Conf::set();
	}
};

class FSCAL2: public CC1101Conf
{
private:
	FSCAL2() = delete;

public:
	bool VCO_CORE_H_EN = false;		// VCO
	uint8_t FSCAL2_VAL = 0x0A;// Frequency synthesizer calibration result register

	FSCAL2(CC1101 &cc) :
			CC1101Conf(cc, REG__FSCAL2)
	{
	}

	~FSCAL2()
	{
	}

	void load()
	{
		CC1101Conf::load();
		VCO_CORE_H_EN = (reg & (1 << 5)) ? true : false;
		FSCAL2_VAL = (reg & 0x1F);
	}

	void set()
	{
		reg = ((VCO_CORE_H_EN & (1 << 5)) ? 0x1 : 0x0);
		reg |= (FSCAL2_VAL & 0x1F);
		CC1101Conf::set();
	}
};

class FSCAL1: public CC1101Conf
{
private:
	FSCAL1() = delete;

public:
	uint8_t FSCAL1_VAL = 0x20;		// Frequency synthesizer calibration control

	FSCAL1(CC1101 &cc) :
			CC1101Conf(cc, REG__FSCAL1)
	{
	}

	~FSCAL1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FSCAL1_VAL = (reg & 0x3F);
	}

	void set()
	{
		reg = (FSCAL1_VAL & 0x3F);
		CC1101Conf::set();
	}
};

class FSCAL0: public CC1101Conf
{
private:
	FSCAL0() = delete;

public:
	uint8_t FSCAL0_VAL = 0x0D;		// Frequency synthesizer calibration control

	FSCAL0(CC1101 &cc) :
			CC1101Conf(cc, REG__FSCAL0)
	{
	}

	~FSCAL0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FSCAL0_VAL = (reg & 0x7F);
	}

	void set()
	{
		reg = (FSCAL0_VAL & 0x7F);
		CC1101Conf::set();
	}
};

class RCCTRL1: public CC1101Conf
{
private:
	RCCTRL1() = delete;

public:
	uint8_t RCCTRL1_VAL = 0x41;		// RC oscillator configuration

	RCCTRL1(CC1101 &cc) :
			CC1101Conf(cc, REG__RCCTRL1)
	{
	}

	~RCCTRL1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RCCTRL1_VAL = (reg & 0x7F);
	}

	void set()
	{
		reg = (RCCTRL1_VAL & 0x7F);
		CC1101Conf::set();
	}
};

class RCCTRL0: public CC1101Conf
{
private:
	RCCTRL0() = delete;

public:
	uint8_t RCCTRL0_VAL = 0x0;		// RC oscillator configuration

	RCCTRL0(CC1101 &cc) :
			CC1101Conf(cc, REG__RCCTRL0)
	{
	}

	~RCCTRL0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RCCTRL0_VAL = (reg & 0x7F);
	}

	void set()
	{
		reg = (RCCTRL0_VAL & 0x7F);
		CC1101Conf::set();
	}
};

class FSTEST: public CC1101Conf
{
private:
	FSTEST() = delete;

public:
	// Only for testing purposes
	uint8_t FSTEST_VAL = 0x0;

	FSTEST(CC1101 &cc) :
			CC1101Conf(cc, REG__FSTEST)
	{
	}

	~FSTEST()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FSTEST_VAL = reg;
	}
};

class PTEST: public CC1101Conf
{
private:
	PTEST() = delete;

public:
	// Production tests only, enable temp sensor in idle
	uint8_t PTEST_VAL = 0x0;

	PTEST(CC1101 &cc) :
			CC1101Conf(cc, REG__PTEST)
	{
	}

	~PTEST()
	{
	}

	void load()
	{
		CC1101Conf::load();
		PTEST_VAL = reg;
	}
};

class AGCTEST: public CC1101Conf
{
private:
	AGCTEST() = delete;

public:
	AGCTEST(CC1101 &cc) :
			CC1101Conf(cc, REG__AGCTEST)
	{
	}

	~AGCTEST()
	{
	}
};

class TEST2: public CC1101Conf
{
private:
	TEST2() = delete;

public:
	TEST2(CC1101 &cc) :
			CC1101Conf(cc, REG__TEST2)
	{
	}

	~TEST2()
	{
	}
};

class TEST1: public CC1101Conf
{
private:
	TEST1() = delete;

public:
	TEST1(CC1101 &cc) :
			CC1101Conf(cc, REG__TEST1)
	{
	}

	~TEST1()
	{
	}
};

class TEST0: public CC1101Conf
{
private:
	TEST0() = delete;

public:
	TEST0(CC1101 &cc) :
			CC1101Conf(cc, REG__TEST0)
	{
	}

	~TEST0()
	{
	}
};

class PARTNUM: public CC1101Conf
{
private:
	PARTNUM() = delete;

public:
	// Chip partnum
	uint8_t PARTNUM_VAL = 0x0;

	PARTNUM(CC1101 &cc) :
			CC1101Conf(cc, REG__PARTNUM)
	{
	}

	~PARTNUM()
	{
	}

	void load()
	{
		CC1101Conf::load();
		PARTNUM_VAL = reg;
	}
};

class VERSION: public CC1101Conf
{
private:
	VERSION() = delete;

public:
	uint8_t VERSION_VAL = 0x0;

	VERSION(CC1101 &cc) :
			CC1101Conf(cc, REG__VERSION)
	{
	}

	~VERSION()
	{
	}

	void load()
	{
		CC1101Conf::load();
		VERSION_VAL = reg;
	}
};

class FREQEST: public CC1101Conf
{
private:
	FREQEST() = delete;

public:
	uint8_t FREQOFF_EST = 0x0;

	FREQEST(CC1101 &cc) :
			CC1101Conf(cc, REG__FREQEST)
	{
	}

	~FREQEST()
	{
	}

	void load()
	{
		CC1101Conf::load();
		FREQOFF_EST = reg;
	}
};

class LQI: public CC1101Conf
{
private:
	LQI() = delete;

public:
	bool CRC_OK = false;
	uint8_t LQI_EST = 0x0;

	LQI(CC1101 &cc) :
			CC1101Conf(cc, REG__LQI)
	{
	}

	~LQI()
	{
	}

	void load()
	{
		CC1101Conf::load();
		CRC_OK = (reg & (1 << 7)) ? true : false;
		LQI_EST = (reg & 0x7F);
	}
};

class RSSI: public CC1101Conf
{
private:
	RSSI() = delete;

public:
	uint8_t RSSI_VAL = 0x0;

	RSSI(CC1101 &cc) :
			CC1101Conf(cc, REG__RSSI)
	{
	}

	~RSSI()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RSSI_VAL = reg;
	}

	/**
	 * Return dBm value (possitive), must be printed negative
	 */
	uint8_t getdBm()
	{
		this->load();
		RSSI_VAL = reg;

		if(RSSI_VAL >= 0x80)
			return (0xFF - RSSI_VAL)/2 + 74;
		return RSSI_VAL/2 + 74;
	}
};

class MARCSTATE: public CC1101Conf
{
private:
	MARCSTATE() = delete;

public:
	enum STATES {
		STATES_SLEEP = 0,
		STATES_IDLE,
		STATES_XOFF,
		STATES_VCOON_MC,
		STATES_REGON_MC,
		STATES_MANCAL,
		STATES_VCOON,
		STATES_REGON,
		STATES_STARTCAL,
		STATES_BWBOOST,
		STATES_FS_LOCK,
		STATES_IFADCON,
		STATES_ENDCAL,
		STATES_RX,			// 13
		STATES_RX_END,
		STATES_TX_RST,
		STATES_TXRX_SWITCH,
		STATES_RXFIFO_OVERFLOW,
		STATES_FSTXON,
		STATES_TX,
		STATES_TX_END,
		STATES_RXTX_SWITCH,
		STATES_TXFIFO_UNDERFLOW
	};
	uint8_t STATE = 0x0;

	MARCSTATE(CC1101 &cc) :
			CC1101Conf(cc, REG__MARCSTATE)
	{
	}

	~MARCSTATE()
	{
	}

	void load()
	{
		CC1101Conf::load();
		STATE = (reg & 0x1F);
	}
};

class WORTIME1: public CC1101Conf
{
private:
	WORTIME1() = delete;

public:
	uint8_t WORTIME = 0x0;	// High byte wor time

	WORTIME1(CC1101 &cc) :
			CC1101Conf(cc, REG__WORTIME1)
	{
	}

	~WORTIME1()
	{
	}

	void load()
	{
		CC1101Conf::load();
		WORTIME = reg;
	}
};

class WORTIME0: public CC1101Conf
{
private:
	WORTIME0() = delete;

public:
	uint8_t WORTIME = 0x0;	// Low byte wor time

	WORTIME0(CC1101 &cc) :
			CC1101Conf(cc, REG__WORTIME0)
	{
	}

	~WORTIME0()
	{
	}

	void load()
	{
		CC1101Conf::load();
		WORTIME = reg;
	}
};

class PKTSTATUS: public CC1101Conf
{
private:
	PKTSTATUS() = delete;

public:
	bool CRC_OK = false;		// last CRC was OK
	bool CS = false;			// carrier sense, clear on transition to idle
	bool PQT_REACHED = false;	// preamble quality reached
	bool CCA = false;			// channel clear
	bool SFD = false;			// start of frame delimiter
	bool GDO2 = false;			// GDO2 status
	bool GDO0 = false;			// GOD0 status

	PKTSTATUS(CC1101 &cc) :
			CC1101Conf(cc, REG__PKTSTATUS)
	{
	}

	~PKTSTATUS()
	{
	}

	void load()
	{
		CC1101Conf::load();
		CRC_OK = (reg & (1 << 7)) ? true : false;
		CS = (reg & (1 << 6)) ? true : false;
		PQT_REACHED = (reg & (1 << 5)) ? true : false;
		CCA = (reg & (1 << 4)) ? true : false;
		SFD = (reg & (1 << 3)) ? true : false;
		GDO2 = (reg & (1 << 2)) ? true : false;
		GDO0 = (reg & (1 << 0)) ? true : false;
	}
};

class VCO_VC_DAC: public CC1101Conf
{
private:
	VCO_VC_DAC() = delete;

public:
	// Status register for test only
	uint8_t VCO_VC_DAC_VAL = 0x0;

	VCO_VC_DAC(CC1101 &cc) :
			CC1101Conf(cc, REG__VCO_VC_DAC)
	{
	}

	~VCO_VC_DAC()
	{
	}

	void load()
	{
		CC1101Conf::load();
		VCO_VC_DAC_VAL = reg;
	}
};

class TXBYTES: public CC1101Conf
{
private:
	TXBYTES() = delete;

public:
	// Number of bytes in TX fifo
	bool TX_OVERFLOW = false;
	uint8_t TXBYTES_VALUE = 0x0;

	TXBYTES(CC1101 &cc) :
			CC1101Conf(cc, REG__TXBYTES)
	{
	}

	~TXBYTES()
	{
	}

	void load()
	{
		CC1101Conf::load();
		TX_OVERFLOW = (reg & (1 << 7)) ? true : false;
		TXBYTES_VALUE = (reg & 0x7F);
	}
};

class RXBYTES: public CC1101Conf
{
private:
	RXBYTES() = delete;

public:
	// Number of bytes in RX fifo
	bool RX_OVERFLOW = false;
	uint8_t RXBYTES_VALUE = 0x0;

	RXBYTES(CC1101 &cc) :
			CC1101Conf(cc, REG__RXBYTES)
	{
	}

	~RXBYTES()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RX_OVERFLOW = (reg & (1 << 7)) ? true : false;
		RXBYTES_VALUE = (reg & 0x7F);
	}
};

class RCCTRL1_STATUS: public CC1101Conf
{
private:
	RCCTRL1_STATUS() = delete;

public:
	// Value of last RC calibration routine
	uint8_t RCCTRL1 = 0x0;

	RCCTRL1_STATUS(CC1101 &cc) :
			CC1101Conf(cc, REG__RCCTRL1_ST)
	{
	}

	~RCCTRL1_STATUS()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RCCTRL1 = (reg & 0x7F);
	}
};

class RCCTRL0_STATUS: public CC1101Conf
{
private:
	RCCTRL0_STATUS() = delete;

public:
	// Value of last RC calibration routine
	uint8_t RCCTRL0 = 0x0;

	RCCTRL0_STATUS(CC1101 &cc) :
			CC1101Conf(cc, REG__RCCTRL0_ST)
	{
	}

	~RCCTRL0_STATUS()
	{
	}

	void load()
	{
		CC1101Conf::load();
		RCCTRL0 = (reg & 0x7F);
	}
};

class SRES: public CC1101Cmd
{
private:
	SRES() = delete;

public:
	SRES(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SRES)
	{
	}

	~SRES()
	{
	}
};

class SFSTXON: public CC1101Cmd
{
private:
	SFSTXON() = delete;

public:
	SFSTXON(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SFSTXON)
	{
	}

	~SFSTXON()
	{
	}

};

class SXOFF: public CC1101Cmd
{
private:
	SXOFF() = delete;

public:
	SXOFF(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SXOFF)
	{
	}

	~SXOFF()
	{
	}
};

class SCAL: public CC1101Cmd
{
private:
	SCAL() = delete;

public:
	SCAL(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SCAL)
	{
	}

	~SCAL()
	{
	}
};

class SRX: public CC1101Cmd
{
private:
	SRX() = delete;

public:
	SRX(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SRX)
	{
	}

	~SRX()
	{
	}
};

class STX: public CC1101Cmd
{
private:
	STX() = delete;

public:
	STX(CC1101 &cc) :
			CC1101Cmd(cc, CMD__STX)
	{
	}

	~STX()
	{
	}
};

class SIDLE: public CC1101Cmd
{
private:
	SIDLE() = delete;

public:
	SIDLE(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SIDLE)
	{
	}

	~SIDLE()
	{
	}
};

class SWOR: public CC1101Cmd
{
private:
	SWOR() = delete;

public:
	SWOR(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SWOR)
	{
	}

	~SWOR()
	{
	}
};

class SPWD: public CC1101Cmd
{
private:
	SPWD() = delete;

public:
	SPWD(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SPWD)
	{
	}

	~SPWD()
	{
	}
};

class SFRX: public CC1101Cmd
{
private:
	SFRX() = delete;

public:
	SFRX(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SFRX)
	{
	}

	~SFRX()
	{
	}
};

class SFTX: public CC1101Cmd
{
private:
	SFTX() = delete;

public:
	SFTX(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SFTX)
	{
	}

	~SFTX()
	{
	}
};

class SWORRST: public CC1101Cmd
{
private:
	SWORRST() = delete;

public:
	SWORRST(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SWORRST)
	{
	}

	~SWORRST()
	{
	}
};

class SNOP: public CC1101Cmd
{
private:
	SNOP() = delete;

public:
	SNOP(CC1101 &cc) :
			CC1101Cmd(cc, CMD__SNOP)
	{
	}

	~SNOP()
	{
	}
};

class FIFO: public CC1101Cmd
{
private:
	FIFO() = delete;

public:
	FIFO(CC1101 &cc) :
			CC1101Cmd(cc, CMD__FIFO)
	{
	}

	~FIFO()
	{
	}
};

#endif
