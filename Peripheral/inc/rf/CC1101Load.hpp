#ifndef _H_CC1101_LOAD_
#define _H_CC1101_LOAD_

#include "gpio/GPIO.hpp"
#include "rf/CC1101Conf.hpp"
#include "gpio/GPIOInterrupt.hpp"

class CC1101Load
{
	// How to handle all parameters ???
private:
	static constexpr uint8_t SYNC_L_WORD = 0x44;
	static constexpr uint8_t SYNC_H_WORD = 0x44;
	IOCFG0 rxBuffer;
	IOCFG2 chanClear;
	MCSM0 mcsm0;
	MCSM1 mcsm1;
	PKTCTRL0 pktctrl0;
	PKTCTRL1 pktctrl1;
	PKTLEN pktlen;

	SRES reset;
	SFRX sfrx;
	SFTX sftx;
	SRX rx;

	SYNC1 sync1;
	SYNC0 sync0;

	MDMCFG2 mdmcfg2;

	GPIO rxPacket;
	GPIO txReady;
	GPIOInterrupt gpioInterrupt;

	CC1101Load() = delete;

public:
	CC1101Load(CC1101 &cc);

	void setCalibration();
	void setGpio();
	void setPacketFmt();
	void setSyncWord();

	void gotoRx();

};


#endif
