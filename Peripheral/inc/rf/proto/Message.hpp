#ifndef _H_MESSAGE_
#define _H_MESSAGE_

#include <stdint.h>

#include "rf/CC1101.hpp"
#include "rf/CC1101Conf.hpp"

enum class MessageType : uint8_t
{
	MessageType_REQ = 0, MessageType_ACK
};

enum class DataType : uint8_t
{
	DataType_RAW = 0, DataType_BEACON, DataType_TEMP, DataType_SYNC
};

class Message
{
private:
	static const uint8_t MAX_MESSAGE_LENGTH = 64;
	static const uint8_t MAX_DATA_LENGTH = 32;
	static const uint8_t MESSAGE_TYPE_BYTES = 1;
	static const uint8_t MESSAGE_ID_BYTES = 2;
	static const uint8_t DATA_TYPE_BYTES = 1;
	static const uint8_t DATA_OFFSET = MESSAGE_TYPE_BYTES + MESSAGE_ID_BYTES
			+ DATA_TYPE_BYTES;
	/*
	 *  B | (B | B) | B | B....B |
	 *  |   ---|---   |     |-----> data
	 *  |      |      |------------> data TYPE
	 *  |      |----------------> message ID
	 *  |--------------------> message TYPE
	 *   
	 */
	enum Synth {
		Synth_M_TYPE = 0,
		Synth_M_ID_H,
		Synth_M_ID_L,
		Synth_D_TYPE,
		Synth_DATA
	};

	uint8_t len;
	uint8_t buffer[MAX_MESSAGE_LENGTH];
	uint16_t messageId;
	DataType dataType;
	MessageType messageType;
	Synth synthStatus;
	Message(const Message &other) = delete;

public:
	Message();
	~Message();

	/**
	 * Assignment operator.
	 * 	Copy only headers of messages, not data
	 * 	Set message type to ACK
	 *
	 * @param othrMessage	source message to copy headers from
	 *
	 * @return			message with copied headers
	 */
	Message &operator=(const Message &othrMessage);

	/**
	 * Clear content of whole message class
	 */
	void clear();

	/**
	 * Get length of message in bytes
	 *
	 * @return	bytes length
	 */
	uint8_t getLen() const;

	/**
	 * Get pointer to data part of message
	 *
	 * @return	pointer to first byte of data
	 */
	const uint8_t *getData() const;

	/**
	 * Set message type, REQ / RSP
	 *
	 * @param message	type to be set
	 */
	void setMessageType(const MessageType message);

	/**
	 * Get type of message REQ / RSP
	 *
	 * @return	type of message
	 */
	MessageType getMessageType() const;

	/**
	 * Set id of message
	 *
	 * @param messageId	id
	 */
	void setMessageId(const uint16_t messageId);

	/**
	 * Get id of message
	 *
	 * @return	id
	 */
	uint16_t getMessageId() const;

	/**
	 * Set type of data
	 *
	 * @param type
	 */
	void setDataType(const DataType type);

	/**
	 * Get type of data
	 *
	 * @return	type of data
	 */
	DataType getDataType() const;

	/**
	 * Append byte to user data part
	 * @param byte
	 *
	 * @return	true when byte appended
	 * 			false otherwise
	 */
	bool append(uint8_t byte);

	/**
	 * Format message buffer before sending
	 *
	 * @param id	id of message
	 * @param len	length of message (set to external)
	 *
	 * @return		pointer to firt byte of fomated message
	 */
	uint8_t *fmtMessage(uint16_t id, uint8_t *len);

	/**
	 * Synthetize message from stream of bytes
	 *
	 * @param byte	one from received stream
	 *
	 * @return		true when byte added
	 * 				false otherwise
	 */
	bool synthMessage(uint8_t byte);
};

#endif
