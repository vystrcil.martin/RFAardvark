#ifndef _H_STATISTICS_
#define _H_STATISTICS_

#include <stdint.h>
#include "tim/Timer.hpp"

class Statistics : TimerCallback
{
private:
	// TX statistics - packets / bytes
	uint32_t txPackets;
	uint32_t txBytes;
	uint32_t txDropPackets;
	uint32_t txDropBytes;
	// RX statistics - packets / bytes
	uint32_t rxPackets;
	uint32_t rxBytes;
	uint32_t rxDropPackets;
	uint32_t rxDropBytes;

public:
	Statistics();
	~Statistics();

	void rx(uint32_t bytesLen);
	void tx(uint32_t bytesLen);

	void clear();
	void dump();
	void execute();
};

#endif
