#ifndef _H_PROTOCOL_
#define _H_PROTOCOL_

#include "Statistics.hpp"
#include "rf/CC1101.hpp"
#include "rf/CC1101Conf.hpp"
#include "rf/proto/Message.hpp"
#include "tim/Timer.hpp"

class Protocol: TimerCallback
{
private:
	// Max 16 messages send in progress
	static constexpr uint8_t MAX_MESSAGES_IN_BUFFER = 8;

	uint8_t messagesInBuffer = 0;
	bool isAckReceived;
	uint16_t lastMessageId;

	CC1101 &cc;
	FIFO fifo;
	SIDLE sidle;
	STX stx;
	SRX srx;
	SFRX sfrx;
	RXBYTES rxbytes;
	Statistics statistics;
	MARCSTATE marcstate;

	Message recvBuffer[MAX_MESSAGES_IN_BUFFER];
	Protocol();

	bool sendAck(Message &message);

public:
	~Protocol()
	{
	}
	;

	/**
	 * Get singleton instance of Protocol class
	 *
	 * @return	singleton instance
	 */
	static Protocol &getInstance();

	/**
	 * Execute timeout callback
	 *  - release old messages
	 */
	void execute();

	/**
	 * Read message from RF chip
	 */
	void onMessageReceived();

	/**
	 * Print monitoring RX data
	 */
	void printMessage(const Message &message, bool rx = false, uint8_t quality =
			0, uint8_t rssi = 0);

	/**
	 * Send prepared message to RF chip
	 *
	 * @param address	address of target RF
	 * @param message	data to be transfered
	 * @return			true when ok
	 * 					false otherwise
	 */
	bool sendMessage(uint16_t address, Message &message);

	/**
	 * Process one buffered message
	 * @retval	 0 message processed
	 * @retval	 1 message process error
	 * @retval	64 no more messages
	 */
	int processMessage();
};

#endif

