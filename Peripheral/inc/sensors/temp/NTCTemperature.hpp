#ifndef _H_NTC_TEMPERATURE_
#define _H_NTC_TEMPERATURE_

#include "gpio/GPIO.hpp"
#include "stm/stm32f10x_adc.h"
#include "sensors/temp/Temperature.hpp"

class NTCTemperature: protected Temperature
{
private:
	uint16_t adcChannel;
	ADC_InitTypeDef ntcAdcSettings;
	GPIO enableGpio;

	const int VREF = 3300;
	const int BETA = 3470;
	const double R0 = 9800.0;
	const double T0 = 298.15;
	const double KELVIN = 273.15;

public:
	NTCTemperature();
	~NTCTemperature();

	int setupTemperature(uint16_t adcChannel);
	void enable();
	void disable();

	double getTemperature();
};

#endif
