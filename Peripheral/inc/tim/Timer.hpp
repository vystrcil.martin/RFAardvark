#ifndef _H_TIMER_
#define _H_TIMER_

#include "stm/stm32f10x.h"

#define TIMER_CALLBACKS_MAX	16

class TimerCallback;

class Timer
{
private:

	typedef struct {
		TimerCallback *ptr;
		uint32_t time;
	} Callback;

	uint32_t earliest;
	Callback callbacks[TIMER_CALLBACKS_MAX];

	Timer();

public:
	static volatile uint32_t currMs;

	static Timer &getInstance();
	~Timer();

	uint32_t getEarliestTime();
	bool registerCallback(TimerCallback *callback, uint32_t time);
	void executeAction();
};

class TimerCallback
{
protected:
	uint16_t timeout;
	Timer &tim;

public:
	TimerCallback() : tim(Timer::getInstance())
	{
		timeout = 500;
	}

	virtual ~TimerCallback()
	{

	}

	virtual void execute()
	{
	}
};

#endif
