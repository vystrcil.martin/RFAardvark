#include "gpio/GPIOInterrupt.hpp"
#include "proto/Log.hpp"
#include "rf/proto/Protocol.hpp"
#include "stm/stm32f10x.h"
#include "stm/misc.h"

extern "C" void EXTI15_10_IRQHandler()
{
	if(EXTI_GetITStatus(EXTI_Line10) != RESET)
	{
		// Copy message from FIFO (RF channel) to protocol buffer
		Protocol::getInstance().onMessageReceived();
		// Clear interrupt flag
		EXTI_ClearITPendingBit(EXTI_Line10);
	}
}

GPIOInterrupt::GPIOInterrupt(uint32_t extiLine, uint16_t nvicChannel)
{
	// EXTI configuration structure
	EXTI_InitTypeDef extiConfig;
	extiConfig.EXTI_Line = extiLine;
	extiConfig.EXTI_Mode = EXTI_Mode_Interrupt;
	extiConfig.EXTI_Trigger = EXTI_Trigger_Rising;
	extiConfig.EXTI_LineCmd = ENABLE;
	EXTI_Init(&extiConfig);

	// NVIC configuration structure
	NVIC_InitTypeDef nvicConfig;
	nvicConfig.NVIC_IRQChannel = nvicChannel;
	nvicConfig.NVIC_IRQChannelPreemptionPriority = 0x0;
	nvicConfig.NVIC_IRQChannelSubPriority = 0x0;
	nvicConfig.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicConfig);

}
