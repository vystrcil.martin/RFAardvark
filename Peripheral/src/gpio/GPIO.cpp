#include "gpio/GPIO.hpp"
#include "proto/Log.hpp"

GPIO::GPIO(GPIO_TypeDef *port, uint16_t pin) :
		port(port)
{
	pinStruct.GPIO_Pin = pin;
	pinStruct.GPIO_Speed = GPIO_Speed_2MHz;
}

GPIO::~GPIO()
{

}

void GPIO::setADCInput()
{
	pinStruct.GPIO_Mode = GPIO_Mode_AIN;

	GPIO_Init(port, &pinStruct);
}

void GPIO::setOutputPP()
{
	pinStruct.GPIO_Mode = GPIO_Mode_Out_PP;

	GPIO_Init(port, &pinStruct);
}

void GPIO::setInput(uint8_t pullUp)
{
	if(pullUp)
		pinStruct.GPIO_Mode = GPIO_Mode_IPU;
	else
		pinStruct.GPIO_Mode = GPIO_Mode_IPD;

	GPIO_Init(port, &pinStruct);
}

void GPIO::setInterrupt()
{
	printLine("Set interrupt on GPIOB");
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource10);
}

void GPIO::setLevel(uint8_t level)
{
	switch (level)
	{
	case LEVEL_LOW:
		port->BSRR |= (pinStruct.GPIO_Pin << 16);
		break;

	case LEVEL_HIGH:
		port->BSRR |= pinStruct.GPIO_Pin;
		break;

	default:
		break;
	}
}

uint8_t GPIO::getLevel()
{
	return (port->IDR & pinStruct.GPIO_Pin) != 0 ? 1 : 0;
}
