#include "callback/ProtocolProcess.hpp"

ProtocolProcess::ProtocolProcess() :
		protocol(Protocol::getInstance())
{
	timeout = 200;
	tim.registerCallback(this, timeout);
}

ProtocolProcess::~ProtocolProcess()
{

}

void ProtocolProcess::execute()
{
	protocol.processMessage();
	// Restart timeout again
	tim.registerCallback(this, timeout);
}
