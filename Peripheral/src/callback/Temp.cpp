#include "callback/Temp.hpp"
#include "proto/Log.hpp"

Temp::Temp() :
	tim(Timer::getInstance())
{
	state = ENABLE;
	printLine("Temperature monitor init");
	ntc.setupTemperature(ADC_Channel_11);
	tim.registerCallback(this, timeout);
}

Temp::~Temp()
{

}

void Temp::execute()
{
	uint16_t temp;

	switch(state)
	{
	case ENABLE:
		ntc.enable();
		state = READ_TEMP;
		tim.registerCallback(this, waitToMeasure);
		return;

	case READ_TEMP:
		temp = ntc.getTemperature();
		printLine("Temp: ");
		printNr(temp);
		ntc.disable();

		state = ENABLE;
		tim.registerCallback(this, timeout);
		return;

	default:
		state = ENABLE;
		tim.registerCallback(this, timeout);
		return;
	}
}

