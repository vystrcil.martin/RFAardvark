#include "callback/SysLed.hpp"
#include "proto/Log.hpp"

SysLed::SysLed()
: TimerCallback(), sysGPIO(port, pin)
{
	printLine("SysLed init");
	status = false;
	timeout = 500;
	tim.registerCallback(this, timeout);

	sysGPIO.setOutputPP();
	sysGPIO.setLevel(LEVEL_HIGH);
}

SysLed::~SysLed()
{

}

void SysLed::execute()
{
	sysGPIO.setLevel(LEVEL_LOW);
	if(status)
	{
		sysGPIO.setLevel(LEVEL_LOW);
		status = false;
	}
	else
	{
		sysGPIO.setLevel(LEVEL_HIGH);
		status = true;
	}

	tim.registerCallback(this, timeout);
}
