#include "callback/RadioStatus.hpp"
#include "proto/Log.hpp"

RadioStatus::RadioStatus(CC1101 &cc) :
		TimerCallback(), cc(cc), state(cc), rssi(cc), pktstat(cc), rxbytes(cc), txbytes(
				cc), quality(cc), fifo(cc), sidle(cc), sfrx(cc), sftx(cc), srx(cc)
{
	timeout = 1000;
	tim.registerCallback(this, timeout);
}

RadioStatus::~RadioStatus()
{

}

void RadioStatus::execute()
{
	// Refresh all status registers
	state.load();
	pktstat.load();
	rxbytes.load();
	txbytes.load();
	quality.load();

	// Print status
	debugStr("Radio state ");
	printNr(state.STATE);
	debugStr(" rssi: -");
	printNr(rssi.getdBm());
	debugStr("dBm rx: ");
	printNr(rxbytes.RXBYTES_VALUE);
	debugStr(" tx: ");
	printNr(txbytes.TXBYTES_VALUE);
	debugStr(" quality: ");
	printNr(quality.LQI_EST);
	debugStr("\r\n");

	switch(state.STATE)
	{
	case MARCSTATE::STATES_RXFIFO_OVERFLOW:
		debugStr("!!! RX Overflow detected - flush\r\n");
		sfrx.send();
		srx.send();
		break;
	case MARCSTATE::STATES_TXFIFO_UNDERFLOW:
		debugStr("!!! TX Underflow detected - flush\r\n");
		sftx.send();
		srx.send();
		break;
	}

	// Print packet status
	debugStr("PktStat: sfd: ");
	printNr(pktstat.SFD);
	debugStr(" cca: ");
	printNr(pktstat.CCA);
	debugStr(" cs: ");
	printNr(pktstat.CS);
	debugStr(" pqt: ");
	printNr(pktstat.PQT_REACHED);
	debugStr("\r\n");

	// Restart timer again
	tim.registerCallback(this, timeout);
}

