#include "callback/RadioBeacon.hpp"
#include "proto/Log.hpp"
#include "rf/proto/Message.hpp"

RadioBeacon::RadioBeacon(CC1101 &cc) :
		TimerCallback(), cc(cc), protocol(Protocol::getInstance()), fifo(cc), stx(
				cc), sidle(cc)
{
	timeout = 1000;

	message.setMessageType(MessageType::MessageType_REQ);
	message.setDataType(DataType::DataType_BEACON);
}

RadioBeacon::~RadioBeacon()
{

}

/**
 * Send beacon message - broadcast data to all listening units
 */
void RadioBeacon::execute()
{
	// Send message with ID
	printLine("Send Beacon");
	protocol.sendMessage(0, message);
	// Restart beacon again
	tim.registerCallback(this, timeout);
}
