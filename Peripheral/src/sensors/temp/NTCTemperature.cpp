#include <math.h>

#include "proto/Log.hpp"
#include "sensors/temp/NTCTemperature.hpp"

NTCTemperature::NTCTemperature() :
		enableGpio(GPIOA, GPIO_Pin_0)
{

}

NTCTemperature::~NTCTemperature()
{

}

int NTCTemperature::setupTemperature(uint16_t adcChannel)
{
	this->adcChannel = adcChannel;
	RCC_ADCCLKConfig(RCC_PCLK2_Div2);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	GPIO gpio(GPIOA, GPIO_Pin_7);
	gpio.setADCInput();

	ADC_InitTypeDef ntcAdcSettings;
	ntcAdcSettings.ADC_Mode = ADC_Mode_Independent;
	ntcAdcSettings.ADC_ScanConvMode = DISABLE;
	ntcAdcSettings.ADC_ContinuousConvMode = DISABLE;
	ntcAdcSettings.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ntcAdcSettings.ADC_DataAlign = ADC_DataAlign_Right;
	ntcAdcSettings.ADC_NbrOfChannel = 1;

	ADC_RegularChannelConfig(ADC1, this->adcChannel, 1,
	ADC_SampleTime_41Cycles5);

	ADC_Init(ADC1, &ntcAdcSettings);
	ADC_Cmd(ADC1, ENABLE);

	enableGpio.setOutputPP();
	enableGpio.setLevel(LEVEL_HIGH);

	ADC_ResetCalibration(ADC1);
	while (ADC_GetResetCalibrationStatus(ADC1))
		;
	ADC_StartCalibration(ADC1);
	while (ADC_GetCalibrationStatus(ADC1))
		;

	return 0;
}

void NTCTemperature::enable()
{
	enableGpio.setLevel(LEVEL_LOW);
}

void NTCTemperature::disable()
{
	enableGpio.setLevel(LEVEL_HIGH);
}

double NTCTemperature::getTemperature()
{
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) != SET)
	{
	}
	ADC_ClearFlag(ADC1, ADC_FLAG_EOC);

	uint16_t voltage = (ADC_GetConversionValue(ADC1) * VREF) / 0xFFF;
	int resistance = ((VREF - voltage) * 1000 / ((voltage / R0) * 1000.0));
	return ((BETA / log(resistance / (R0 * exp(-(BETA / T0))))) - KELVIN) * 10;
}
