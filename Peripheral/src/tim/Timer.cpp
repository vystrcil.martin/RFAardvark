#include "tim/Timer.hpp"
#include "proto/Log.hpp"

extern "C"
{
#include "stm/core_cm3.h"
#include "stm/stm32f10x_rcc.h"
}

volatile uint32_t Timer::currMs;

Timer::Timer()
{
	printLine("Timer init");
	Timer::currMs = 0;

	RCC_ClocksTypeDef clocks;
	RCC_GetClocksFreq(&clocks);

	earliest = 0xFFFFFFFF;
	for(int i = 0; i < TIMER_CALLBACKS_MAX; i++)
	{
		callbacks[i].ptr = nullptr;
		callbacks[i].time = 0;
	}

	(void) SysTick_Config(clocks.HCLK_Frequency / 1000);
}

Timer::~Timer()
{

}

Timer &Timer::getInstance()
{
	static Timer instance;

	return instance;
}

uint32_t Timer::getEarliestTime()
{
	return earliest;
}

/**
 * Register callback into array, if there is a place
 *
 * \param	callback - pointer of object
 * \retval	true when OK
 * 			false when not registered
 */
bool Timer::registerCallback(TimerCallback *callback, uint32_t time)
{
	time += Timer::currMs;
	bool rc = false;

	for(int i = 0; i < TIMER_CALLBACKS_MAX; i++)
	{
		if(callbacks[i].ptr == nullptr)
		{
			if(earliest > time)
				earliest = time;

			callbacks[i].ptr = callback;
			callbacks[i].time = time;
			rc = true;
			break;
		}
	}

	return rc;
}

/**
 * Execute action if some is scheduled
 * Update last
 */
void Timer::executeAction()
{
	earliest = 0xFFFFFFFF;

	for(int i = 0; i < TIMER_CALLBACKS_MAX; i++)
	{
		if(callbacks[i].ptr != nullptr)
		{
			if(callbacks[i].time < currMs)
			{
				callbacks[i].ptr->execute();
				callbacks[i].ptr = nullptr;
			}
			else if(callbacks[i].time < earliest)
			{
				// Find the other earliest
				earliest = callbacks[i].time;
			}
		}
	}
}
