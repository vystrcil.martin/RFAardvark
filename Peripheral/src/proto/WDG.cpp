#include "proto/WDG.hpp"

#include "stm/stm32f10x_iwdg.h"

WDG::WDG()
{
	// Enable internal low speed oscillator
	RCC_LSICmd(ENABLE);
	// Wait until LSI is stable
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
	{
	}
	// Enable write to IWDG config registers
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	// Set IWDG clocks prescaller
	IWDG_SetPrescaler(IWDG_Prescaler_64);
	// Set timeout for IWDG
	IWDG_SetReload(0xFFF);
	// Disable write to IWDG config registers
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Disable);
	// Enable IWDG - start ticking
	IWDG_Enable();
}

WDG::~WDG()
{

}

WDG &WDG::getInstance()
{
	// Create WDG class instance
	static WDG instance;
	// return instance
	return instance;
}

void WDG::refresh()
{
	IWDG_ReloadCounter();
}
