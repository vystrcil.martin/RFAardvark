#include "proto/SPI.hpp"
#include "proto/Log.hpp"

#include "stm/stm32f10x_gpio.h"
#include "stm/stm32f10x_rcc.h"
#include "stm/stm32f10x_spi.h"

#define SPI_FLUSH_AND_WAIT \
	while (! (SPI_IFACE->SR & SPI_I2S_FLAG_TXE))\
	{}\
	while (!(SPI_IFACE->SR & SPI_I2S_FLAG_RXNE))\
	{}\
	while (SPI_IFACE->SR & SPI_I2S_FLAG_BSY)\
	{}

SPI::SPI() : chipSelect(GPIOA, PIN_SS)
{
	// Enable GPIO clocks
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	// Enable SPI1 peripheral
	RCC_APB2PeriphClockCmd(SPI_CLK, ENABLE);

	GPIO_InitTypeDef spiGpio;

	// Init data and clock on SPI bus
	spiGpio.GPIO_Pin = PIN_CLK | PIN_MOSI;
	spiGpio.GPIO_Speed = GPIO_Speed_50MHz;
	spiGpio.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &spiGpio);

	// Init data and clock on SPI bus
	spiGpio.GPIO_Pin = PIN_MISO;
	spiGpio.GPIO_Speed = GPIO_Speed_50MHz;
	spiGpio.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &spiGpio);

	// Init chip select on SPI bus
	/*spiSS.GPIO_Pin = PIN_SS;
	spiSS.GPIO_Speed = GPIO_Speed_50MHz;
	spiSS.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &spiSS);*/
	chipSelect.setOutputPP();
	chipSelect.setLevel(LEVEL_HIGH);

	// Init SPI peripheral1
	SPI_InitTypeDef spi;

	spi.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	spi.SPI_Mode = SPI_Mode_Master;
	spi.SPI_DataSize = SPI_DataSize_8b;
	spi.SPI_CPOL = SPI_CPOL_Low;
	spi.SPI_CPHA = SPI_CPHA_1Edge;
	spi.SPI_NSS = SPI_NSS_Soft;
	spi.SPI_FirstBit = SPI_FirstBit_MSB;
	spi.SPI_CRCPolynomial = 0;
	spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;

	SPI_Init(SPI_IFACE, &spi);
	SPI_Cmd(SPI_IFACE, ENABLE);

	while(SPI_I2S_GetFlagStatus(SPI_IFACE, SPI_I2S_FLAG_OVR))
	{
		printLine("SPI: Init - overrun set");
		SPI_I2S_ReceiveData(SPI_IFACE);
	}
}

SPI::~SPI()
{

}

/**
 * Read value from register
 *
 * @param reg	register address (with R/W, burst set)
 * @return		value of register
 */
uint8_t SPI::sendRead(uint8_t reg)
{
	uint8_t data;

	this->readBurst(reg, &data, 1);

	// Return received data from SPI data register
	return data;
}

/**
 * Write value to register
 *
 * @param reg		register address (with R/W, burst set)
 * @param value		data to be written
 */
void SPI::sendWrite(uint8_t reg, uint8_t value)
{
	this->sendBurst(reg, &value, 1);
}

/**
 * Send data to SPI register in burst mode
 *
 * @param reg	register address to write to (with R/W, burst set)
 * @param data	data pointer to write
 * @param len	length of data to write
 *
 * @return		>0 length of data written
 * 				=0 on fail
 */
void SPI::sendBurst(uint8_t reg, uint8_t *data, uint8_t len)
{
	chipSelect.setLevel(LEVEL_LOW);

	// Wait for MISO to go low
	while(GPIO_ReadInputDataBit(GPIOA, PIN_MISO) != 0)
	{}

	// Send header byte
	SPI_I2S_SendData(SPI_IFACE, reg);
	SPI_FLUSH_AND_WAIT
	// Read status register back
	SPI_I2S_ReceiveData(SPI_IFACE);

	if(len > 1)
	{
		// Send length of packet as first byte
		SPI_I2S_SendData(SPI_IFACE, len);
		SPI_FLUSH_AND_WAIT
		// Read status register back
		SPI_I2S_ReceiveData(SPI_IFACE);
	}

	// Send all data bytes
	for(int i = 0; i < len; ++i)
	{
		SPI_I2S_SendData(SPI_IFACE, *data++);
		SPI_FLUSH_AND_WAIT
		SPI_I2S_ReceiveData(SPI_IFACE);

	}

	chipSelect.setLevel(LEVEL_HIGH);
}

/**
 * Read data from SPI register in burst mode
 *
 * @param reg	register address
 * @param data	pointer where to store data
 * @param len	size of data memory
 *
 * @return		>0 bytes read
 * 				=0 on fail
 */
void SPI::readBurst(uint8_t reg, uint8_t *data, uint8_t len)
{
	chipSelect.setLevel(LEVEL_LOW);

	// Wait for MISO to go low
	while(GPIO_ReadInputDataBit(GPIOA, PIN_MISO) != 0)
	{}

	SPI_I2S_SendData(SPI_IFACE, reg);
	SPI_FLUSH_AND_WAIT
	// Clear SPI1->DR buffer
	SPI_I2S_ReceiveData(SPI_IFACE);

	for(int i = 0; i < len; i++)
	{
		// Send fake data (clock for read)
		SPI_I2S_SendData(SPI_IFACE, 0x0);
		SPI_FLUSH_AND_WAIT
		*data++ = (uint8_t) SPI_I2S_ReceiveData(SPI_IFACE);
	}

	// Deselect radio interface from comm
	chipSelect.setLevel(LEVEL_HIGH);
}
