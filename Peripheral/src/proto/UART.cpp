#include "proto/UART.hpp"

#include "stm/stm32f10x_usart.h"

/**
 * Initialize UART1 as debugging output
 */
UART::UART()
{
	// Enable clocks toward USART1, GPIO PA09,PA10
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA,
			ENABLE);

	GPIO_InitTypeDef usartTx;
	usartTx.GPIO_Pin = GPIO_Pin_9;
	usartTx.GPIO_Speed = GPIO_Speed_10MHz;
	usartTx.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &usartTx);

	GPIO_InitTypeDef usartRx;
	usartRx.GPIO_Pin = GPIO_Pin_10;
	usartRx.GPIO_Speed = GPIO_Speed_10MHz;
	usartRx.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &usartRx);

	USART_InitTypeDef usart;
	usart.USART_BaudRate = 115200;
	usart.USART_WordLength = USART_WordLength_8b;
	usart.USART_StopBits = USART_StopBits_1;
	usart.USART_Parity = USART_Parity_No;
	usart.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

	USART_ClockInitTypeDef usartClk;
	USART_ClockStructInit(&usartClk);
	usartClk.USART_Clock = USART_Clock_Enable;

	USART_Init(USART1, &usart);
	USART_Cmd(USART1, ENABLE);
}

UART::~UART()
{

}

UART &UART::getInstance()
{
	static UART instance;

	return instance;
}

void UART::sendMessage(const char *message, uint16_t length)
{

	for (int ch = 0; ch < length; ch++)
	{
		USART_SendData(USART1, message[ch]);

		while (!USART_GetFlagStatus(USART1, USART_FLAG_TC))
		{
		}
	}
}
