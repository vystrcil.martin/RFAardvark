#include "proto/Log.hpp"

#include <string.h>

char* itoa(int i, char b[])
{
	char const digit[] = "0123456789";
	char* p = b;
	if (i < 0)
	{
		*p++ = '-';
		i *= -1;
	}

	int shifter = i;
	do
	{ //Move to where representation ends
		++p;
		shifter = shifter / 10;
	} while (shifter);

	*p = '\0';
	do
	{ //Move back, inserting digits as u go
		*--p = digit[i % 10];
		i = i / 10;
	} while (i);

	return b;
}

char *itoa_hex(uint8_t byte, char b[])
{
	char const digits[] = "0123456789ABCDEF";
	char *p = b;
	uint8_t nibble = 0x0;
	// Take high nibble first
	nibble = (byte & 0xF0) >> 4;
	*p++ = '0';
	*p++ = 'x';
	*p++ = digits[nibble];
	nibble = (byte & 0x0F);
	*p++ = digits[nibble];
	*p = '\0';

	return b;
}

char *itoa_hex(uint16_t byte, char b[])
{
	char const digits[] = "0123456789ABCDEF";
	char *p = b;
	uint8_t nibble = 0x0;
	// Take high nibble first
	*p++ = '0';
	*p++ = 'x';
	for(uint8_t idx = 0; idx < 4; idx++)
	{
		// Take nibble from correct position
		nibble = (byte & (0xF << 4 * (3 - idx))) >> (4* (3 - idx));
		*p++ = digits[nibble];
	}
	*p = '\0';

	return b;
}

/*
 * Print constant string from message
 * Find it's size similar to strlen
 */

void debugStr(const char *message)
{
	UART::getInstance().sendMessage(message, strlen(message));
}

void printLine(const char *message)
{
	const char *newline = "\r\n";

	UART::getInstance().sendMessage(message, strlen(message));
	UART::getInstance().sendMessage(newline, strlen(newline));
}

void printNr(uint32_t num)
{
	char numStr[64];
	itoa(num, numStr);

	UART::getInstance().sendMessage(numStr, strlen(numStr));
}

void printHex(uint8_t num)
{
	char numStr[8];
	itoa_hex(num, numStr);

	UART::getInstance().sendMessage(numStr, strlen(numStr));
}

void printHex(uint16_t num)
{
	char numStr[8];
	itoa_hex(num, numStr);
	UART::getInstance().sendMessage(numStr, strlen(numStr));
}
