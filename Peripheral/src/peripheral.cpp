#include "rf/CC1101.hpp"
#include "rf/CC1101Conf.hpp"
#include "rf/CC1101Load.hpp"
#include "callback/SysLed.hpp"
#include "callback/Temp.hpp"
#include "callback/ProtocolProcess.hpp"
#include "callback/RadioBeacon.hpp"
#include "callback/RadioStatus.hpp"

#include "gpio/GPIO.hpp"
#include "proto/Log.hpp"
#include "proto/SPI.hpp"
#include "proto/WDG.hpp"
#include "tim/Timer.hpp"
#include "sensors/temp/NTCTemperature.hpp"

#include "stm/stm32f10x_conf.h"
#include "stm/stm32f10x_usart.h"

extern "C" void SysTick_Handler(void)
{
	Timer::currMs++;

	if (Timer::currMs > Timer::getInstance().getEarliestTime())
	{
		Timer::getInstance().executeAction();
	}
}

void printBootReason()
{
	debugStr("CSR raw value: ");
	printNr(RCC->CSR);

	// Restart cause by internal watchdog
	if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST))
		debugStr("\r\n!!!IWDG restart");
	// TODO: understand what this flag stands for
	// Restart caused by software
	if(RCC_GetFlagStatus(RCC_FLAG_SFTRST))
		debugStr("\r\n###SW restart");
	// Restart caused by lack of power
	if(RCC_GetFlagStatus(RCC_FLAG_PORRST))
		debugStr("\r\n@@@PWR restart");
	// Carriage return
	debugStr("\r\n");
	// Clear restart flags
	RCC_ClearFlag();
}

int main()
{
	// Enable clocks toward GPIOC
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	// Enable clocks toward GPIOB
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);

	/*
	 * Configure pin C13 (PC13)
	 *
	 *   VCC --- LED --- R --- PC13
	 *
	 * Active low
	 */

	printLine("\r\nAArdvark started");
	printBootReason();
	SysLed sysled;
	CC1101 &cc = CC1101::getInstance();

	CC1101Load loader(cc);
	loader.setSyncWord();
	loader.setCalibration();
	loader.setGpio();
	loader.setPacketFmt();
	loader.gotoRx();

	RadioStatus status(cc);
	RadioBeacon beacon(cc);

	// For testing purposes only, read part of serial number
	uint32_t *pUID = (uint32_t *)0x1FFFF7E8;
	if((*pUID) == 107609936)
	{
		printLine("---> MASTER");
		beacon.execute();
	}
	else
	{
		printLine("---> SLAVE");
	}
	WDG &wdg = WDG::getInstance();
	Protocol &prot = Protocol::getInstance();

	while (true)
	{
		// Process next received packet
		prot.processMessage();
		// Refresh internal watchdog
		wdg.refresh();
	}

	return 0;
}

