#include "rf/CC1101.hpp"
#include "proto/Log.hpp"

#include "stm/stm32f10x.h"

#define TX_FIFO_BYTE	0x00
#define TX_FIFO_BURST	0x40
#define RX_FIFO_BYTE	0x80
#define RX_FIFO_BURST	0xC0

CC1101::CC1101()
{
	printLine("CC11010 init");

	/*
	 * Most Significant Bit first
	 * Header byte
	 * 	---------------------------------------
	 * 	| R/W bit | Burst bit | 6-bit address |
	 * 	---------------------------------------
	 */

}

CC1101::~CC1101()
{

}

/**
 * Get single instance of CC1101 handler
 *
 * @return		instance
 */
CC1101 &CC1101::getInstance()
{
	static CC1101 cc;
	return cc;
}

/**
 * Send command specified by class CMD
 *
 * @param command	command instance
 * @return			status byte
 */
uint8_t CC1101::sendCmd(CMD command)
{
	uint8_t cmd = 0x80;
	cmd |= (command & 0x3F);	// register num has 6 b

	return spi.sendRead(cmd);
}

/**
 * Read value of register
 *
 * @param reg	register instance
 * @return		register value
 */
uint8_t CC1101::readReg(REG reg)
{
	uint8_t cmd = 0xC0;
	cmd |= (reg & 0x3F);	// register num has 6 bits

	return spi.sendRead(cmd);
}

/**
 * Write data to register
 *
 * @param reg	register instance
 * @param data	new value of register
 */
void CC1101::writeReg(REG reg, uint8_t data)
{
	spi.sendWrite(reg, data);
}

/**
 * Send len data bytes to RF
 *
 * @param data	pointer to data source
 * @param len	length of data source
 * @return		>0	nr of bytes send
 * 				=0	on fail
 */
void CC1101::sendData(uint8_t *data, uint8_t len)
{
	// Send data to RF (transmit)
	uint8_t reg = REG__FIFO;

	// If burst access
	if(len > 1)
		reg |= TX_FIFO_BURST;

	spi.sendBurst(reg, data, len);
}

/**
 * Read up to len data bytes from RF
 *
 * @param data	pointer to data buffer
 * @param len	max length of buffer
 * @return		>0 nr of bytes read
 * 				=0 on fail
 */
void CC1101::readData(uint8_t *data, uint8_t len)
{
	// Read data from RF (receive)
	uint8_t reg = RX_FIFO_BYTE | REG__FIFO;

	if(len > 1)
		reg |= RX_FIFO_BURST;

	spi.readBurst(reg, data, len);
}
