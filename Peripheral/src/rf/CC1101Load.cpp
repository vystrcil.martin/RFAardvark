#include "rf/CC1101Load.hpp"

#include "proto/Log.hpp"

CC1101Load::CC1101Load(CC1101 &cc) :
		rxBuffer(cc), chanClear(cc), mcsm0(cc), mcsm1(cc), pktctrl0(cc), pktctrl1(
				cc), pktlen(cc), reset(cc), sfrx(cc), sftx(cc), rx(cc), sync1(
				cc), sync0(cc), mdmcfg2(cc), rxPacket(GPIOB, GPIO_Pin_10), txReady(GPIOB,
				GPIO_Pin_11), gpioInterrupt(EXTI_Line10, EXTI15_10_IRQn)
{
	reset.send();
	sfrx.send();
	sftx.send();
}

void CC1101Load::setCalibration()
{
	mcsm0.FS_AUTOCAL = 3; // calibrate on every 4th transition rx -> tx -> rx -> idle
	mcsm0.set();

	mcsm1.RXOFF_MODE = 0x0; // go to IDLE mode (manual RX start)
	mcsm1.TXOFF_MODE = 0x3; // go to RX mode
	mcsm1.set();

	debugStr("Autocal: ");
	printNr(mcsm0.FS_AUTOCAL);
	debugStr("\r\n");

	debugStr("Read RX auto off ");
	printNr(mcsm1.RXOFF_MODE);
	debugStr("\r\n");
}

void CC1101Load::setGpio()
{
	rxBuffer.GDO0_CFG = 0x1;
	rxBuffer.set();
	chanClear.GDO2_CFG = 0x9;
	chanClear.set();

	debugStr("GDO0: ");
	printNr(rxBuffer.GDO0_CFG);
	debugStr("\r\n");
	rxPacket.setInput(1);
	rxPacket.setInterrupt();
	// register GPIO interrupt 

	debugStr("GDO2: ");
	printNr(chanClear.GDO2_CFG);
	debugStr("\r\n");
	txReady.setInput(1);
}

void CC1101Load::setPacketFmt()
{
	pktctrl0.WHITE_DATA = true;	// enable data whitening
	pktctrl0.PKT_FMT = 0x0;		// use fifo for both RX / TX
	pktctrl0.CRC_EN = true;		// CRC calculate on TX, check on RX
	//pktctrl0.LEN_CFG = 0x0;	// fixed packet length
	pktctrl0.LEN_CFG = 0x1;		// variadic packet length
	pktctrl0.set();

	pktctrl1.PQT = 0x4;			// sync word preceded with preamble
	pktctrl1.CRC_AUTOFLUSH = true;
	pktctrl1.APPEND_STATUS = true;
	pktctrl1.set();

	mdmcfg2.SYNC_MODE = 7;		// 30/32 + carrier-sense above threshold 
	mdmcfg2.set();

	pktlen.LEN = 64;
	pktlen.set();
}

void CC1101Load::setSyncWord()
{
	sync1.DATA = SYNC_H_WORD;
	sync0.DATA = SYNC_L_WORD;

	sync1.set();
	sync0.set();
}

void CC1101Load::gotoRx()
{
	rx.send();
}
