#include "rf/proto/Protocol.hpp"

Protocol::Protocol() :
		isAckReceived(true), lastMessageId(0), cc(CC1101::getInstance()), fifo(
				cc), sidle(cc), stx(cc), srx(cc), sfrx(cc), rxbytes(cc), marcstate(cc)
{
	timeout = 500;
}

/* State machine of protocol
 *      ----------         ---------------        -------------------           -----------
 *     | Send REQ |  ---> | Set message ID| ---> | Store messsage ID |  ---->  |  RF Send  |
 *      ----------         ---------------        -------------------           -----------
 *                                                                                 ^   |
 *                                                                                 |   |
 *                                                                                 |   V
 *                                                -------------------        --------------------
 *                                               | Remove message ID | <--- |    Wait for ACK    |
 *                                                -------------------        --------------------
 *
 *
 *
 *
 */

/**
 * Execute timeout callback
 *  - release old messages
 */
void Protocol::execute()
{
	if (!isAckReceived)
	{
		debugStr("Timeout waiting for ACK ");
		printHex(lastMessageId);
		debugStr("\r\n");
	}
}

/**
 * Read message from RF chip
 */
void Protocol::onMessageReceived()
{
	uint8_t byte;
	// Clear buffer with recv message
	recvBuffer[messagesInBuffer].clear();
	// Reread size of message (bytes in RX FIFO)
	rxbytes.load();

	if (rxbytes.RXBYTES_VALUE < 1)
	{
		debugStr("!!! Empty message\r\n");
		// Revert back to RX mode
		srx.send();
		return; //no bytes available
	}

	// First received byte is length of message
	uint8_t length = fifo.send();
	// Increment RX statistics
	statistics.rx(length);
	for (uint8_t start = 0; start < length; start++)
	{
		// Read one byte from RX FIFO
		byte = fifo.send();
		// Append byte to message class in buffer
		if(! recvBuffer[messagesInBuffer].synthMessage(byte))
		{
			debugStr("Failed to append more bytes to message\r\n");
			// Flush rest of FIFO
			sfrx.send();
			// Message handled, goto RX again
			srx.send();
			return;
		}
	}
	uint8_t rssi = fifo.send();
	uint8_t quality = fifo.send();
	printMessage(recvBuffer[messagesInBuffer], true, quality, rssi);
	messagesInBuffer++;
	// Flush any other data received in FIFO (some other packet etc)
	sfrx.send();
	// Message handled, goto RX again
	srx.send();
}

/**
 * Print monitoring RX data
 */
void Protocol::printMessage(const Message &message, bool rx, uint8_t quality,
		uint8_t rssi)
{
	uint8_t dLen = message.getLen();
	if(rx)
		debugStr("Data rx:");
	else
		debugStr("Data tx:");
	// Length of user data (payload)
	debugStr("\r\n\tdLen: ");
	printNr(dLen);
	debugStr("\r\n\tdata: ");
	// Print message type ACK / REQ
	printHex((uint8_t) message.getMessageType());
	debugStr(" ");
	// Print message ID (high byte)
	printHex((uint8_t) (message.getMessageId() >> 8));
	debugStr(" ");
	// Print message ID (low byte)
	printHex((uint8_t) (message.getMessageId()));
	debugStr(" ");
	// Print message type (BEACON / TEMP / RAW / etc)
	printHex((uint8_t) message.getDataType());
	dLen -= 4;
	// Format print user data
	for (uint8_t start = 0; start < dLen; start++)
	{
		printHex(*(message.getData() + start));
		debugStr(" ");
	}
	if(rx)
	{
		// Measured quality
		debugStr("\r\n\tcrc: ");
		printHex((uint8_t)((quality & 0x8) ? 1 : 0));
		debugStr(" qual: ");
		printHex((uint8_t)(quality & 0x7));
		debugStr(" rssi: ");
		// Measured RSSI
		printHex(rssi);
	}
	debugStr("\r\n");
}

/**
 * Send prepared message to RF chip
 *
 * @param address	address of target RF
 * @param message	data to be transfered
 * @return			true when ok
 * 					false otherwise
 */
bool Protocol::sendMessage(uint16_t address, Message &message)
{
	uint8_t *buffer;
	uint8_t bufferLen;

	// Unused parameter
	(void) address;
	// Get message buffer
	buffer = message.fmtMessage(++lastMessageId, &bufferLen);
	printMessage(message);
	sidle.send();
	// Send data from message
	fifo.sendBcast(buffer, bufferLen);
	stx.send();
	// Increment TX statistics
	statistics.tx(bufferLen);
	isAckReceived = false;
	// Schedule wait for ACK
	tim.registerCallback(this, timeout);

	return true;
}

/**
 * Process one buffered message
 * @retval	 0 message processed
 * @retval	 1 message process error
 * @retval	64 no more messages
 */
int Protocol::processMessage()
{
	if (messagesInBuffer == 0)
	{
		// No more messages to be processed
		return 64;
	}

	__disable_irq();
	Message &lastMessage = recvBuffer[--messagesInBuffer];
	debugStr("Process message from buffer ");
	printHex(lastMessage.getMessageId());
	debugStr("\r\n");
	if (lastMessage.getMessageType() == MessageType::MessageType_ACK)
	{
		debugStr("Received ACK for ");
		printHex(lastMessage.getMessageId());
		debugStr("\r\n");

		if (lastMessage.getMessageId() == lastMessageId)
			isAckReceived = true;

		// Remove message ID from messageIdsInProcess
		__enable_irq();
		return 0;
	}

	// Increment TX statistics
	statistics.tx(lastMessage.getLen());
	if (!sendAck(lastMessage))
	{
		debugStr("Failed to send ACK for ");
		printHex(lastMessage.getMessageId());
		debugStr("\r\n");
	}

	// TODO: execute callback with message data

	__enable_irq();
	return 0;
}

/**
 * Get singleton instance of Protocol class
 *
 * @return	singleton instance
 */
Protocol &Protocol::getInstance()
{
	static Protocol protocol;
	return protocol;
}

/**
 * Private functions implementation
 */
bool Protocol::sendAck(Message &message)
{
	uint8_t messageLength;
	uint8_t *pMessageData;
	// Copy headers from last message to response message
	Message response;
	response = message;

	debugStr("Send ACK for: ");
	printHex((uint16_t) message.getMessageId());
	debugStr("\r\n");

	pMessageData = response.fmtMessage(message.getMessageId(), &messageLength);
	sidle.send();
	// Wait until in IDLE state
	do
	{
		debugStr("Wait until in IDLE.\r\n");
		marcstate.load();
	}while(marcstate.get() != MARCSTATE::STATES_IDLE);

	// Copy data from message to FIFO buffer
	fifo.sendBcast(pMessageData, messageLength);
	// Change to TX state
	stx.send();
	// Wait until packet sent (RX state)
	do
	{
		debugStr("Wait until back in RX.\r\n");
		marcstate.load();
	}while(marcstate.get() != MARCSTATE::STATES_RX);

	return true;
}

