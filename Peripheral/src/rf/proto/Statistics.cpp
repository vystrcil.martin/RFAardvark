#include "proto/Log.hpp"
#include "rf/proto/Statistics.hpp"

Statistics::Statistics() :
		txPackets(0), txBytes(0), txDropPackets(0), txDropBytes(0), rxPackets(
				0), rxBytes(0), rxDropPackets(0), rxDropBytes(0)
{
	timeout = 10000;
	tim.registerCallback(this, timeout);
}

Statistics::~Statistics()
{

}

void Statistics::rx(uint32_t bytesLen)
{
	++rxPackets;
	rxBytes += bytesLen;
}

void Statistics::tx(uint32_t bytesLen)
{
	++txPackets;
	txBytes += bytesLen;
}

void Statistics::clear()
{
	__disable_irq();
	// clear TX statistics
	txPackets = 0;
	txBytes = 0;
	txDropPackets = 0;
	txDropBytes = 0;
	// clear RX statistics
	rxPackets = 0;
	rxBytes = 0;
	rxDropPackets = 0;
	rxDropBytes = 0;
	__enable_irq();
}

void Statistics::dump()
{
	printLine("Statistics");
	debugStr("\tRX packets:");
	printNr(rxPackets);
	debugStr("\t\tTX packets:");
	printNr(txPackets);
	debugStr("\r\n\tRX bytes:");
	printNr(rxBytes);
	debugStr("\t\tTX bytes:");
	printNr(txBytes);
	debugStr("\r\n");
}

void Statistics::execute()
{
	this->dump();
	tim.registerCallback(this, timeout);
}
