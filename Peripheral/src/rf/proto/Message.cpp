#include "rf/proto/Message.hpp"
#include "proto/Log.hpp"

Message::Message() :
		len(DATA_OFFSET), messageId(0), dataType(DataType::DataType_RAW), messageType(
				MessageType::MessageType_ACK), synthStatus(Synth_M_TYPE)
{

}

Message::~Message()
{

}

Message &Message::operator=(const Message &othrMessage)
{
	if(this == &othrMessage)
		// Assign to itself is not supported
		return *this;

	dataType = othrMessage.getDataType();
	messageId = othrMessage.getMessageId();
	messageType = MessageType::MessageType_ACK;

	return *this;
}

/**
 * Clear content of whole message class
 */
void Message::clear()
{
	len = DATA_OFFSET;
	dataType = DataType::DataType_RAW;
	synthStatus = Synth::Synth_M_TYPE;
}

/**
 * Get length of message in bytes
 *
 * @return	bytes length
 */
uint8_t Message::getLen() const
{
	return len;
}

/**
 * Get pointer to data part of message
 *
 * @return	pointer to first byte of data
 */
const uint8_t *Message::getData() const
{
	return buffer;
}

/**
 * Set message type, REQ / RSP
 *
 * @param message	type to be set
 */
void Message::setMessageType(const MessageType message)
{
	this->messageType = message;
}

/**
 * Get type of message REQ / RSP
 *
 * @return	type of message
 */
MessageType Message::getMessageType() const
{
	return messageType;
}

/**
 * Set id of message
 *
 * @param messageId	id
 */
void Message::setMessageId(const uint16_t messageId)
{
	this->messageId = messageId;
}

/**
 * Get id of message
 *
 * @return	id
 */
uint16_t Message::getMessageId() const
{
	return messageId;
}

/**
 * Set type of data
 *
 * @param type
 */
void Message::setDataType(const DataType type)
{
	this->dataType = type;
}

/**
 * Get type of data
 *
 * @return	type of data
 */
DataType Message::getDataType() const
{
	return dataType;
}

/**
 *
 * @param byte
 *
 * @return	true when byte appended
 * 			false otherwise
 */
bool Message::append(uint8_t byte)
{
	if (len >= MAX_MESSAGE_LENGTH)
		return false;

	buffer[len++] = byte;
	return true;
}

/**
 * Format message buffer before sending
 *
 * @param id	id of message
 * @param len	length of message (set to external)
 *
 * @return		pointer to firt byte of fomated message
 */
uint8_t *Message::fmtMessage(uint16_t id, uint8_t *len)
{
	uint8_t offset = 0;

	// set message length as output
	*len = this->len;
	// set message ID persistent
	this->messageId = id;
	// message type
	buffer[offset] = (uint8_t) messageType;
	offset += MESSAGE_TYPE_BYTES;
	// message id
	buffer[offset] = (uint8_t) (id >> 8);
	buffer[offset + 1] = (uint8_t) id;
	offset += MESSAGE_ID_BYTES;
	// data type
	buffer[offset] = (uint8_t) dataType;
	offset += DATA_TYPE_BYTES;

	return buffer;
}

/**
 * Synthetize message from stream of bytes
 *
 * @param byte	one from received stream
 *
 * @return		true when byte added
 * 				false otherwise
 */
bool Message::synthMessage(uint8_t byte)
{
	bool retval = true;

	// Set appropriate part of Message, based on status
	switch(synthStatus)
	{
	case Synth_M_TYPE:
		messageType = (MessageType) byte;
		synthStatus = Synth_M_ID_H;
		break;

	case Synth_M_ID_H:
		messageId = (byte << 8);
		synthStatus = Synth_M_ID_L;
		break;

	case Synth_M_ID_L:
		messageId |= (uint8_t) byte;
		synthStatus = Synth_D_TYPE;
		break;

	case Synth_D_TYPE:
		dataType = (DataType) byte;
		synthStatus = Synth_DATA;
		break;

	case Synth_DATA:
	default:
		if(len >= MAX_MESSAGE_LENGTH)
			retval = false;
		buffer[len++] = byte;
		break;
	}

	return retval;
}

