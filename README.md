# RFAardvark

RFAardvark is base IOT project which places basic radio communication.

## Dependencies
Toolchain, to be installed from path: https://gitlab.com/vystrcil.martin/stm32toolchain
STLink flash loader - stlink-tools

## STM32 Toolchain
1, download https://gitlab.com/vystrcil.martin/stm32toolchain
2. add path to stm32toolchain to PATH

## STlink tools
Install using apt install stlink-tools

## Compile STlink tools
> git clone https://github.com/texane/stlink stlink.git
> $cd stlink.git
> ./autogen.sh
> ./configure
> make
> sudo cp st-flash /usr/bin
> cd ..
> sudo cp *.rules /etc/udev/rules.d
> sudo restart udev




