cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project (RFAardvarkCentral VERSION 0.1 LANGUAGES C CXX ASM)
set(PROJECT_DESCRIPTION "Central part of RFAadvark project to support IOT")

####
# Set CROSS_COMPILE
####
set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_ASM_COMPILER gcc)
set(CMAKE_C_COMPILER gcc)
set(CMAKE_CXX_COMPILER g++)
set(CMAKE_C_COMPILER_ID GNU)
set(CMAKE_CXX_COMPILER_ID GNU)
set(CMAKE_C_COMPILER_FORCED TRUE)
set(CMAKE_CXX_COMPILER_FORCED TRUE)
set(CMAKE_OBJCOPY objcopy)
set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "")
set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "")

####
# Set compilator specifications
#  -std=cxx11
####
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

####
# Check if all required libraries installed
####

# mosquitto c library
find_library(MOSQUITTO_LIB mosquitto)
if (NOT MOSQUITTO_LIB)
	message(FATAL_ERROR "mosquitto library not found")
endif()

# mosquitto c++ library
find_library(MOSQUITTOPP_LIB mosquittopp)
if (NOT MOSQUITTOPP_LIB)
	message(FATAL_ERROR "mosquittopp library not found")
endif()

####
# Set source files
####
set(CENTRAL_SRCS
	src/central.cpp
	src/event/Event.cpp
	src/mqtt/MQTT.cpp
	src/sensor/Temperature.cpp
	src/zone/Zone.cpp
	src/zone/Objects.cpp
	)

####
# Set header files
####
set(CENTRAL_HEADERS
	inc/event/Event.hpp
	inc/mqtt/MQTT.hpp
	inc/sensor/Sensor.hpp
	inc/sensor/Temperature.hpp
	inc/zone/Zone.hpp
	inc/zone/Objects.hpp
	)

####
# Set project include directory
###
get_filename_component(INCLUDE_PATH "inc" ABSOLUTE)
include_directories(
    ${INCLUDE_PATH}
)

####
# Set compilation options
#  Disable some c++ options
####
set(GNU_PARAMS
	-pedantic
	-Wall
	-Wextra
	-Werror
)

####
# Create executable target
#  Set it's compile options
#  Set it's linker options
####
add_executable(${PROJECT_NAME} ${CENTRAL_SRCS} ${CENTRAL_HEADERS})
target_link_libraries(${PROJECT_NAME} mosquittopp)
