#ifndef RF_H_EVENT_
#define RF_H_EVENT_

/**
 * Class which is registered to any specified fd.
 * Wait for data (read) until awailable, then execute callback
 *
 * Use system specific select / poll
 */
class Event
{
private:
    Event();

    // Disable copy constructor, assign operator
    Event(const Event &) = delete;
    Event& operator=(const Event &) = delete;
public:
    ~Event();

    void registerCallback();
};

#endif
