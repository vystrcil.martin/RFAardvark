#ifndef RF_H_MQTT_
#define RF_H_MQTT_

#include <iostream>
#include <mosquittopp.h>
#include <string>

using namespace std;

class MQTT : public mosqpp::mosquittopp
{
private:
    const char *id;
    bool connected;
    string target;

    MQTT() = delete;
    explicit MQTT(const char *id, bool clean);

public:
    explicit MQTT(string target);
    virtual ~MQTT();

    bool isConnected() const;

    virtual void on_connect(int rc);
    virtual void on_disconnect(int rc);
    virtual void on_publish(int mid);
    virtual void on_message(const struct mosquitto_message *message);
    virtual void on_subscribe(int mid, int qos_count, const int *granted_qos);
    virtual void on_unsubscribe(int mid);
    virtual void on_log(int level, const char *str);
    virtual void on_error();
};

#endif
