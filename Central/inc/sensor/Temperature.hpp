#ifndef RF_H_SENSOR_TEMPERATURE_
#define RF_H_SENSOR_TEMPERATURE_

#include "sensor/Sensor.hpp"

using namespace std;

// Data structure to transmit through MQTT
struct TemperatureData
{
    uint32_t timestamp;
    int temp;
};

class Temperature : public Sensor
{
private:
    MQTT &mqt;
    string root;
    string path;
    string topic;
    struct TemperatureData data;

    Temperature() = delete;

public:
    Temperature(MQTT &mqt, const string &root, const string &path);
    virtual ~Temperature();

    // Send new temperature to server
    virtual void update();
    virtual void subscribe();
    virtual const string &getTopic() const;
    virtual void handle(const struct mosquitto_message *message);
};

#endif
