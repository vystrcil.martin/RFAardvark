#ifndef RF_H_SENSOR_
#define RF_H_SENSOR_

#include <mosquittopp.h>
#include <string>

#include "mqtt/MQTT.hpp"

using namespace std;

enum SENSOR_TYPE
{
    SENSOR_TYPE_TEMPERATURE,
    SENSOR_TYPE_ALL
};

class Sensor
{
protected:
    Sensor() {}

public:
    Sensor(MQTT &mqt, string root, string path);
    virtual ~Sensor() {}

    // Send new status from local instance to server
    virtual void update() = 0;
    virtual void subscribe() = 0;
    virtual const string &getTopic() const = 0;
    virtual void handle(const struct mosquitto_message *message) = 0;
};

#endif

