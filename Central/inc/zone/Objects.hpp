#ifndef RF_H_OBJECTS_
#define RF_H_OBJECTS_

#include <map>
#include <memory>
#include <string>

#include "sensor/Sensor.hpp"

using namespace std;

class Objects
{

private:
    map<string, shared_ptr<Sensor>> topics;

    Objects();

public:
    ~Objects();

    static Objects &getInstance();
    bool insertTopic(const string &topic, shared_ptr<Sensor> sensor);
    shared_ptr<Sensor> getSensorByTopic(const string &topic);

};

#endif
