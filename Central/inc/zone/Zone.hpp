#ifndef RF_H_ZONE_
#define RF_H_ZONE_

#include <map>
#include <memory>

#include "sensor/Sensor.hpp"
#include "sensor/Temperature.hpp"

using namespace std;

class Zone
{

private:
    string name;
    map<int, shared_ptr<Sensor>> sensors;

    Zone() = delete;

public:
    Zone(string name);

    bool addTemperature(shared_ptr<Temperature> temp);
    shared_ptr<Temperature> getTemperature();

    /**
     * @brief updateAll
     */
    void updateAll();
};

#endif
