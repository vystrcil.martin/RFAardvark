#include "mqtt/MQTT.hpp"
#include "zone/Objects.hpp"

MQTT::MQTT(const char *id, bool clean) : id(id)
{
    connected = false;
    mosqpp::lib_init();
}

MQTT::MQTT(string target) : MQTT(nullptr, true)
{
    if(connect_async(target.c_str()) != MOSQ_ERR_SUCCESS)
    {
        std::cerr << "Async connect failed" << std::endl;
    }

    if(loop_start() != MOSQ_ERR_SUCCESS)
    {
        std::cerr << "Loop start failed" << std::endl;
    }
}

MQTT::~MQTT()
{
    mosqpp::lib_cleanup();
    this->loop_stop();
    std::cerr << "Destructor MQTT" << std::endl;
}

bool MQTT::isConnected() const
{
    return connected;
}

void MQTT::on_connect(int rc)
{
    if(rc != 0)
    {
        std::cerr << "Connect failed" << std::endl;
        return;
    }

    std::cout << "Connected" << std::endl;
    connected = true;
}

void MQTT::on_disconnect(int rc)
{
    std::cout << "Disconnected" << std::endl;

    if(rc != 0)
        std::cerr << "Disconnect failed" << std::endl;
}

void MQTT::on_publish(int rc)
{
    std::cout << "Published" << std::endl;

    switch(rc)
    {
    case MOSQ_ERR_SUCCESS:
        break;

    case MOSQ_ERR_NOMEM:
        std::cerr << "Out of memory" << std::endl;
        break;

    case MOSQ_ERR_PROTOCOL:
        std::cerr << "Protocol error" << std::endl;
        break;
    }
}

void MQTT::on_message(const struct mosquitto_message *message)
{
    std::cout << "Message received" << std::endl;
    std::cout << "MID: " << message->mid << std::endl;
    std::cout << "Topic: " << message->topic << std::endl;

    std::shared_ptr<Sensor> sensor =
            Objects::getInstance().getSensorByTopic(message->topic);

    sensor->handle(message);
}

void MQTT::on_subscribe(int mid, int qos_count, const int *granted_qos)
{
    std::cout << "Subscribe ..." << std::endl;
    std::cout << "MID: " << mid << " q: " << qos_count
              << " opts: " << granted_qos << std::endl;
}

void MQTT::on_unsubscribe(int mid)
{
    std::cout << "Unsubscribe done " << mid << std::endl;
}

void MQTT::on_log(int level, const char *str)
{
    std::cout << ">>> log: " << level << " " << str << std::endl;
}

void MQTT::on_error()
{
    std::cerr << "Error happened" << std::endl;
}
