#include <chrono>
#include <cstring>
#include <iostream>
#include <thread>

#include "mqtt/MQTT.hpp"
#include "sensor/Temperature.hpp"
#include "zone/Zone.hpp"

int main(int argc, char *argv[])
{
    string root("/main-temp");
    std::cout << "Start central" << std::endl;

    MQTT mqt("192.168.169.10");
    Zone main(root);
    main.addTemperature(make_shared<Temperature>(mqt, root, "/ntc"));

    // Wait until connected
    while(! mqt.isConnected())
    {
        std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(200));
    }

    while(true)
    {
        mqt.loop(2000, 1);
        std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(2000));
    }
}
