#include "zone/Objects.hpp"
#include "zone/Zone.hpp"

Zone::Zone(string name) : name(name)
{

}
/**
 * @brief Zone::addTemperature
 * @param temp
 * @return
 */
bool Zone::addTemperature(shared_ptr<Temperature> temp)
{
    temp->subscribe();
    Objects::getInstance().insertTopic(temp->getTopic(), temp);
    sensors.insert(make_pair(SENSOR_TYPE_TEMPERATURE, temp));
    return true;
}

/**
 * @brief Zone::getTemperature
 * @return shared_ptr with Temperature instance
 */
shared_ptr<Temperature> Zone::getTemperature()
{
    return static_pointer_cast<Temperature>(
                sensors.at(SENSOR_TYPE_TEMPERATURE));
}

/**
 * Iterate through all registered sensors and update their values
 * @brief Zone::updateAll
 */
void Zone::updateAll()
{
    auto iter = sensors.begin();

    while(iter != sensors.end())
    {
        (iter)->second->update();
        iter++;
    }
}
