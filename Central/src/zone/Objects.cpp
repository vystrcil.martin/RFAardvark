#include "zone/Objects.hpp"

Objects::Objects()
{

}

Objects::~Objects()
{

}

Objects &Objects::getInstance()
{
    static Objects instance;

    return instance;
}

bool Objects::insertTopic(const string &topic, shared_ptr<Sensor> sensor)
{
    topics.insert(make_pair(topic, sensor));
    return true;
}

shared_ptr<Sensor> Objects::getSensorByTopic(const string &topic)
{
    auto item = topics.find(topic);
    if( item == topics.end())
        return nullptr;

    return item->second;
}
