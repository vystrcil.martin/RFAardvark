#include <cstring>

#include "mqtt/MQTT.hpp"
#include "sensor/Temperature.hpp"
#include "zone/Objects.hpp"

Temperature::Temperature(MQTT &mqt, const string &root, const string &path) :
    mqt(mqt), root(root), path(path)
{
    topic.append(root).append(path);
}

Temperature::~Temperature()
{

}

void Temperature::update()
{
    const char *message = "20.5";

    std::cout << "Temperature update" << std::endl;
    if(mqt.publish(nullptr, path.c_str(), strlen(message), message) != 0)
    {
        std::cerr << "Message publish failed" << std::endl;
    }
}

void Temperature::subscribe()
{
    std::cout << "Temperature subscribe " << topic << std::endl;
    if(mqt.subscribe(nullptr, topic.c_str(), 0) != 0)
    {
        // TODO: throw an exception
        cerr << "Subscribe to " << topic << " failed" << endl;
    }
}

const string &Temperature::getTopic() const
{
    return topic;
}

void Temperature::handle(const struct mosquitto_message *message)
{
    cout << "Received: " << message->payloadlen << " bytes" << endl;
    if(sizeof (data) != message->payloadlen)
    {
        cerr << "Message not of temperature type" << endl;
        return;
    }

    memcpy(&data, message->payload, message->payloadlen);
}
